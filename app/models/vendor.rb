class Vendor < ApplicationRecord

  has_many :items
  has_many :purchase_orders
  belongs_to :tax, optional: true

  # Validation
  validates :name, :uniqueness => true

  def country_name
    countries = CS.countries
    country = countries.select { |key, value| key.to_s.match(self.country_code) } if self.country_code.present?
    return country[self.country_code.to_sym] if self.country_code.present?
  end

  def state_name
    states = CS.states(self.country_code)
    state = states.select { |key, value| key.to_s.match(self.province_code) } if self.province_code.present?
    return state[self.province_code.to_sym] if self.province_code.present?
  end

  def tax_code
    if self.tax_id.present? && self.tax_id != 0
      tax = Tax.find(self.tax_id)
      tax.code
    else
      ""
    end
  end



  private

  def self.import_from_csv(file)
    unless file.content_type == "text/csv"
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      vendors = []
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        vendor = new

        vendors << { name: row["Vendor Name"], address1: row["Vendor Address 1"], address2: row["Vendor Address 2"], city_code: row["City"],
          country_code: row["Country"], province_code: row["Province"], postal_code: row["Postal Code"], phone_number: row["Phone Number"],
          fax_number: row["Fax Number"], contact_name: row["Contact Person"], email: row["Email"]}
      end
      vendors.each do |vendor|
        if vendor[:province_code].present?
          vendor[:tax_id] = 1 if vendor[:province_code] == "Ontario"
          vendor[:tax_id] = 2 if vendor[:province_code] == "Quebec"
          vendor[:tax_id] = 3 if vendor[:province_code] == "Alberta"
          vendor[:tax_id] = 4 if vendor[:province_code] == "British Columbia"
          vendor[:tax_id] = 5 if vendor[:province_code] == "Prince Edward Island"
          vendor[:tax_id] = 6 if vendor[:province_code] == "Saskatchewan"
          vendor[:tax_id] = 7 if vendor[:province_code] == "Manitoba"
          vendor[:tax_id] = 8 if vendor[:province_code] == "Nova Scotia	"
          vendor[:tax_id] = 9 if vendor[:province_code] == "New Brunswick"
          vendor[:tax_id] = 10 if vendor[:province_code] == "Yukon"
          vendor[:tax_id] = 11 if vendor[:province_code] == "Northwest Territories"
          vendor[:tax_id] = 12 if vendor[:province_code] == "Nunavut"
          vendor[:tax_id] = 13 if vendor[:province_code] == "Newfoundland and Labrador"
        end
        vendor[:country_code] = get_country_code(vendor[:country_code]) if vendor[:country_code].present?
        vendor[:province_code] = get_state_code(vendor[:country_code], vendor[:province_code]) if vendor[:province_code].present?
      end

      @vendors = Vendor.create(vendors)
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def self.get_country_code(country_name)
    countries = CS.countries
    country = countries.select { |key, value| value.to_s.match(country_name) }
    return country.keys[0].to_s
  end

  def self.get_state_code(country_name, state_name)
    states = CS.states(country_name)
    state = states.select { |key, value| value.to_s.match(state_name) }
    return state.keys[0].to_s
  end

  def self.to_csv
    attributes = %w{id name address1 address2 city_code state_name country_name postal_code phone_number fax_number contact_name email tax_code}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |vendor|
        csv << attributes.map{ |attr| vendor.send(attr) }
      end
    end
  end

end