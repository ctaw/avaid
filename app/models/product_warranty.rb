class ProductWarranty < ApplicationRecord
  has_many :quotes
  has_many :quote_items
end