class Quote < ApplicationRecord

  belongs_to :customer
  has_many :quote_items
  accepts_nested_attributes_for :quote_items, reject_if: :all_blank, allow_destroy: true

  has_many :items, :through => :quote_items
  has_many :shipments, :dependent => :destroy
  has_many :send_logs, :dependent => :destroy
  belongs_to :tax, optional: true
  belongs_to :user, optional: true
  belongs_to :term, optional: true
  has_many :orders, :dependent => :destroy

end
