class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :role
  has_many :audits
  has_many :purchase_orders
  has_many :quotes
  has_many :orders

  def full_name
    if self.first_name.present? && self.last_name.present?
      "#{self.first_name } #{self.last_name}"
    else
      self.email
    end
  end

  def user_role
    if self.role_id.present?
      role = Role.find(self.role_id)
      role.name
    else
      ""
    end
  end

  private

  def self.to_csv
    attributes = %w{id email user_role first_name last_name direct_number is_active}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

end
