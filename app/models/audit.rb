class Audit < ApplicationRecord
  before_create :generate_and_set_code

  belongs_to :item, optional: true
  belongs_to :user, optional: true
  belongs_to :customer, optional: true


  validates :item_ids, :presence => false

  def customer_name
    customer = Customer.find(self.customer_id)
    return customer.name
  end

  def purchase_by
    user = User.find(self.user_id)
    return user.email
  end

  private

  def generate_and_set_code
    unless self.code.present?
      self.code = "A#{SecureRandom.rand(9999999)}"
      set_code if Audit.find_by(code: self.code)
    end
  end

  def self.to_csv
    attributes = %w{id code customer_name purchase_by received_by audited_date received_date audit_start note receive_note no_of_items }

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |audit|
        csv << attributes.map{ |attr| audit.send(attr) }
      end
    end
  end



end
