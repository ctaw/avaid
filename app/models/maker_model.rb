class MakerModel < ApplicationRecord

  belongs_to :maker, optional: true
  has_many :items
  has_many :quote_items

end