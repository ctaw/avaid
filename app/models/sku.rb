class Sku < ApplicationRecord

  private

  def self.import_from_csv(file)
    if file.content_type == "text/csv"
    else
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      products = []
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        product = new
        products << { name: row["SKU"], description: row["Item Description"] }
      end
      Sku.create!(products)
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end