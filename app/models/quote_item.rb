class QuoteItem < ApplicationRecord

  belongs_to :quote
  belongs_to :item, optional: true
  belongs_to :maker, optional: true
  belongs_to :maker_model, optional: true
  belongs_to :product_warranty, optional: true
  belongs_to :grade, optional: true
end