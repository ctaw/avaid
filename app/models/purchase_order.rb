class PurchaseOrder < ApplicationRecord

  before_create :set_purchase_order_number

  # Relationship
  belongs_to :vendor
  belongs_to :payment_method
  belongs_to :user
  belongs_to :tax, optional: true
  belongs_to :warehouse

  has_many :purchase_order_items
  accepts_nested_attributes_for :purchase_order_items, reject_if: :all_blank, allow_destroy: true

  def country_name
    countries = CS.countries
    country = countries.select { |key, value| key.to_s.match(self.country_code) }
    return country[self.country_code.to_sym]
  end

  def state_name
    states = CS.states(self.country_code)
    state = states.select { |key, value| key.to_s.match(self.province_code) }
    return state[self.province_code.to_sym]
  end

  private

  def set_purchase_order_number
    year = Date.current.year
    start_zeroes = 00
    po = PurchaseOrder.last
    po_id = po.present? ? po.id.to_i+1 : 1



    self.purchase_order_number = "#{year}-#{start_zeroes}#{po_id}"
    set_purchase_order_number if PurchaseOrder.find_by(purchase_order_number: self.purchase_order_number)
  end

end