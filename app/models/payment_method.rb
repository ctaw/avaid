class PaymentMethod < ApplicationRecord
  has_many :purchase_orders
end