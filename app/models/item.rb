require 'csv'

class Item < ApplicationRecord

  # Relationship
  belongs_to :status, optional: true
  belongs_to :category, optional: true
  belongs_to :sub_category, optional: true
  belongs_to :maker, optional: true
  belongs_to :maker_model, optional: true
  belongs_to :grade, optional: true
  belongs_to :condition_type, optional: true
  belongs_to :vendor, optional: true
  belongs_to :warehouse, optional: true
  belongs_to :audit, optional: true

  has_many :quote_items
  has_many :purchase_orders
  has_one :item_category, :dependent => :destroy
  has_many :order_items


  def category_name
    if self.category_id.present? && self.category_id != 0
      cat = Category.find(self.category_id)
      cat.name
    else
      ""
    end
  end

  def sub_category_name
    if self.sub_category_id.present? && self.sub_category_id != 0
      sub = SubCategory.find(self.sub_category_id)
      sub.name
    else
      ""
    end
  end

  def make
    if self.maker_id.present? && self.maker_id != 0
      maker = Maker.find(self.maker_id)
      maker.name
    else
      ""
    end
  end

  def model
    if self.maker_model_id.present? && self.maker_model_id != 0
      mmodel = MakerModel.find(self.maker_model_id)
      mmodel.name
    else
      ""
    end
  end

  def location
    if self.warehouse_id.present? && self.warehouse_id != 0
      ware = Warehouse.find(self.warehouse_id)
      ware.name
    else
      ""
    end
  end

  def vendor_name
    if self.vendor_id.present? && self.vendor_id != 0
      vendor = Vendor.find(self.vendor_id)
      vendor.name
    else
      ""
    end
  end

  def condition
    if self.condition_type_id.present? && self.condition_type_id != 0
      condition = ConditionType.find(self.condition_type_id)
      condition.name
    else
      ""
    end
  end

  def item_status
    if self.status_id.present? && self.status_id != 0
      status = Status.find(self.status_id)
      status.name
    else
      ""
    end
  end

  def item_grade
    if self.grade_id.present? && self.grade_id != 0
      grade = Grade.find(self.grade_id)
      grade.name
    else
      ""
    end
  end

  def po_number
    if self.purchase_order_id.present? && self.purchase_order_id != 0
      "P-#{self.purchase_order_id}"
    else
      ""
    end
  end

  def audit_code
    if self.audit_id.present? && self.audit_id != 0
      audit = Audit.find(self.audit_id)
      audit.code
    else
      ""
    end
  end

  private

  def self.import_from_csv(file)

    audit_data = []
    item_data = []
    maker_data = []
    maker_model_data = []
    vendor_data = []
    category_data = []
    grade_data = []

    if file.content_type == "text/csv"
      audit_note = []

      CSV.foreach(file.path, headers: true).with_index(1) do |row, ln|
        audit_note.push(row['Notes']) if !row['Notes'].nil?

        audit_data << { code: row['Load# *'], note: audit_note }
        maker_data << { name: row['System manufacturer'] }
        maker_model_data << { make: row['System manufacturer'], name: row['System model'] }
        category_data << { name: row['System chassis type'] }
        vendor_data << { name: row['Client *'] }
        grade_data << { name: row['Grade'] }

        item_data << {
          audit_code: row['Load# *'],
          serial: row['System serial'], hdd_serial: row['Disk serial'],
          maker_id: row['System manufacturer'], maker_model_id:row['System model'],
          description: row['CPU model'],
          grade_id: row['Grade'],
          vendor_id: row['Client *'],
          category_id: row['System chassis type']
          }

      end
    end

    saving_data(audit_data, item_data, maker_data, maker_model_data, vendor_data, category_data, grade_data)

  end

  def self.saving_data(audit_data, item_data, maker_data, maker_model_data, vendor_data, category_data, grade_data)

    audit_data.uniq.each do |audit|
      unless Audit.where(code: audit[:code]).exists?
        Audit.create!(code: audit[:code], note: audit[:note])
      end
    end

    maker_data.uniq.each do |maker|
      if Maker.where(name: maker[:name]).exists?
        @res_maker = Maker.where(name: maker[:name]).last
      else
        @res_maker = Maker.create(name: maker[:name])
      end
    end

    maker_model_data.uniq.each do |mmodel|
      makers = Maker.where(name: mmodel[:make])
      if makers.exists?
        makers.each do |maker|
          unless MakerModel.where(name: mmodel[:make]).exists?
            MakerModel.create(maker_id: maker.id, name: mmodel[:name])
          end
        end
      end
    end

    category_data.uniq.each do |category|
      unless Category.where(name: category[:name]).exists?
        Category.create(name: category[:name])
      end
    end

    vendor_data.uniq.each do |vendor|
      unless Vendor.where(name: vendor[:name]).exists?
        Vendor.create(name: vendor[:name])
      end
    end

    grade_data.uniq.each do |grade|
      unless Grade.where(name: grade[:name]).exists?
        Grade.create(name: grade[:name])
      end
    end


    item_ids = []
    item_data.uniq.each do |item|
      category = Category.where(name: item[:category_id]).first
      make = Maker.where(name: item[:maker_id]).first
      make_model = MakerModel.where(name: item[:maker_model_id]).first
      vendor = Vendor.where(name: item[:vendor_id]).first
      grade = Grade.where(name: item[:grade_id]).first

      if Audit.where(code: item[:audit_code]).exists?
        @audit = Audit.where(code: item[:audit_code]).last
        @item = Item.create!(serial: item[:serial], hdd_serial: item[:hdd_serial], maker_id: make.id, maker_model_id: make_model.id, category_id: category.id, sub_category: nil, vendor_id: vendor.id, grade_id: grade.id, description: item[:description], audit_id: @audit.id)
      else
        @item = Item.create!(serial: item[:serial], hdd_serial: item[:hdd_serial], maker_id: make.id, maker_model_id: make_model.id, category_id: category.id, sub_category: nil, vendor_id: vendor.id, grade_id: grade.id, description: item[:description])
      end

      item_ids << @item.id
      audits = Audit.where(code: item[:audit_code])
      audits.each do |audit|
        audit.update(item_ids: item_ids)
      end
    end

  end


  def self.import_wipe_csv(file)
    item_data = []
    maker_data = []
    vendor_data = []
    if file.content_type == "text/csv"
      CSV.foreach(file.path, headers: true).with_index(1) do |row, ln|
        maker_data << { name: row['Motherboard Vendor'] }
        vendor_data << { name: row["Vendor"] }

        item_data << {
          serial: row["Drive Serial"],
          maker_id: row["Motherboard Vendor"],
          part_number: row["Motherboard Product"],
          description: row['CPU 1'],
          vendor_id: row["Vendor"]
        }
      end
    end

    saving_items(maker_data, vendor_data, item_data)
  end

  def self.saving_items(maker_data, vendor_data, item_data)
    maker_data.uniq.each do |maker|
      unless Maker.where(name: maker[:name]).exists?
        Maker.create(name: maker[:name])
      end
    end

    vendor_data.uniq.each do |vendor|
      unless Vendor.where(name: vendor[:name]).exists?
        Vendor.create(name: vendor[:name])
      end
    end

    item_ids = []

    item_data.uniq.each do |item|
      make = Maker.where(name: item[:maker_id]).first
      vendor = Vendor.where(name: item[:vendor_id]).first

      @item = Item.create!(serial: item[:serial], part_number: item[:part_number], maker_id: make.id, vendor_id: vendor.id, description: item[:description])
    end

  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def self.to_csv
    attributes = %w{id sku part_number serial hdd_serial category_name sub_category_name make model condition item_status item_grade location vendor_name description qty cost cost_of_addons cost_of_freight po_number quote_id audit_code }

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |item|
        csv << attributes.map{ |attr| item.send(attr) }
      end
    end
  end

end