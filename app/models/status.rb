class Status < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  # Relationship
  has_many :items
end