class Order < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :delivery_method, optional: true
  belongs_to :delivery_type, optional: true
  belongs_to :quote
  belongs_to :order_status
  has_many :order_items
  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true
end