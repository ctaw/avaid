class Grade < ApplicationRecord
  has_many :items
  has_many :quote_items
end