class Customer < ApplicationRecord

  has_many :items
  has_many :purchase_orders
  has_many :quotes
  has_many :audits
  belongs_to :tax, optional: true

  # Validation
  validates :name, :uniqueness => true

  def country_name
    countries = CS.countries
    country = countries.select { |key, value| key.to_s.match(self.country_code) }
    return country[self.country_code.to_sym]
  end

  def state_name
    states = CS.states(self.country_code)
    state = states.select { |key, value| key.to_s.match(self.province_code) }
    return state[self.province_code.to_sym]
  end

  private

  def self.import_from_csv(file)
    unless file.content_type == "text/csv"
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      customers = []
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        customer = new
        shipment = new

        customers << { name: row["Customer Name"], address1: row["Customer Address 1"], address2: row["Customer Address 2"], city_code: row["City"],
          country_code: row["Country"], province_code: row["Province"], postal_code: row["Postal Code"], phone_number: row["Phone Number"],
          fax_number: row["Fax Number"], contact_name: row["Contact Name"], email: row["Email"]}
      end
      customers.each do |customer|

        if customer[:province_code].present?
          customer[:tax_id] = 1 if customer[:province_code] == "Ontario"
          customer[:tax_id] = 2 if customer[:province_code] == "Quebec"
          customer[:tax_id] = 3 if customer[:province_code] == "Alberta"
          customer[:tax_id] = 4 if customer[:province_code] == "British Columbia"
          customer[:tax_id] = 5 if customer[:province_code] == "Prince Edward Island"
          customer[:tax_id] = 6 if customer[:province_code] == "Saskatchewan"
          customer[:tax_id] = 7 if customer[:province_code] == "Manitoba"
          customer[:tax_id] = 8 if customer[:province_code] == "Nova Scotia	"
          customer[:tax_id] = 9 if customer[:province_code] == "New Brunswick"
          customer[:tax_id] = 10 if customer[:province_code] == "Yukon"
          customer[:tax_id] = 11 if customer[:province_code] == "Northwest Territories"
          customer[:tax_id] = 12 if customer[:province_code] == "Nunavut"
          customer[:tax_id] = 13 if customer[:province_code] == "Newfoundland and Labrador"
        end

        customer[:country_code] = get_country_code(customer[:country_code]) if customer[:country_code].present?
        customer[:province_code] = get_state_code(customer[:country_code], customer[:province_code]) if customer[:province_code].present?
      end

      @customers = Customer.create(customers)
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def self.get_country_code(country_name)
    countries = CS.countries
    country = countries.select { |key, value| value.to_s.match(country_name) }
    return country.keys[0].to_s
  end

  def self.get_state_code(country_name, state_name)
    states = CS.states(country_name)
    state = states.select { |key, value| value.to_s.match(state_name) }
    return state.keys[0].to_s
  end


end
