class OrderStatus < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  # Relationship
  has_many :orders
end