class Maker < ApplicationRecord

  # validates :name, presence: true, uniqueness: true

  has_many :items
  has_many :maker_models, :dependent => :destroy
  accepts_nested_attributes_for :maker_models, reject_if: :all_blank, allow_destroy: true

  has_many :quote_items
end