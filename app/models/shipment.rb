class Shipment < ApplicationRecord
  belongs_to :quote

  def country_name
    countries = CS.countries
    country = countries.select { |key, value| key.to_s.match(self.country_code) }
    return country[self.country_code.to_sym]
  end

  def state_name
    states = CS.states(self.country_code)
    state = states.select { |key, value| key.to_s.match(self.province_code) }
    return state[self.province_code.to_sym]
  end
  
end