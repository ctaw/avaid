class Category < ApplicationRecord

  has_many :items
  has_many :item_categories
  has_many :sub_categories, :dependent => :destroy
  accepts_nested_attributes_for :sub_categories, reject_if: :all_blank, allow_destroy: true

  # validates :name, presence: true, uniqueness: true


end