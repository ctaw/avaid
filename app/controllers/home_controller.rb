class HomeController < ApplicationController
  def index
    redirect_to '/admin/dashboard'
  end
end
