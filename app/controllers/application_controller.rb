class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    "/admin/dashboard"
  end

  def layout_by_resource
    if devise_controller?
      "login"
    end
  end

end
