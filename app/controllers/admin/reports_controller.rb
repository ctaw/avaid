class Admin::ReportsController < AdminController

  def index
    if params[:report].present?
      if params[:report][:name] == "Sales"
        start_date = Time.zone.parse(params[:report][:start_date])
        end_date = Time.zone.parse(params[:report][:end_date])
        @result = Quote.where( 'created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day)
        @sales = Quote.select("id, user_id, sum(subtotal) as total_sale").where( 'created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day).group("id, user_id")

        @sales_by_salesperson = Quote.joins(:user).where( 'quotes.created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day).group("users.email").sum(:subtotal)
        quote_items = QuoteItem.where(quote_id: @result.pluck(:id))

        item_categories = []
        quote_items.each do |quote_item|
          item_categories << { Category.find(quote_item.item.category_id).name => quote_item.qty} if quote_item.item_id.present?
        end

        @item_sold_by_category = item_categories.group_by { |h| h.keys.first }.map do |k, v|
          Array[k, v.reduce(0) { |acc, n| acc + n.values.first.to_i }]
        end


        @average_by_salesperson = []
        @result.each do |res|
          avg = @result.sum(:subtotal)/@result.count
          if res.user_id.present?
            @average_by_salesperson << [res.user.email, avg.to_i]
          end
        end
      end


    else
      start_date = Time.zone.now
      end_date = Time.zone.now
      @result = Quote.where( 'created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day)
      @sales = Quote.select("id, user_id, sum(subtotal) as total_sale").where( 'created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day).group("id, user_id")

      @sales_by_salesperson = Quote.joins(:user).where( 'quotes.created_at BETWEEN ? AND ?', start_date.beginning_of_day, end_date.end_of_day).group("users.email").sum(:subtotal)
      quote_items = QuoteItem.where(quote_id: @result.pluck(:id))


      item_categories = []
      quote_items.each do |quote_item|
        item_categories << { Category.find(quote_item.item.category_id).name => quote_item.qty} if quote_item.item_id.present?
      end


      @item_sold_by_category = item_categories.group_by { |h| h.keys.first }.map do |k, v|
        Array[k, v.reduce(0) { |acc, n| acc + n.values.first.to_i }]
      end


      @average_by_salesperson = []
      @result.each do |res|
        avg = @result.sum(:subtotal)/@result.count
        if res.user_id.present?
          @average_by_salesperson << [res.user.email, avg.to_i]
        end
      end

    end

  end

  def convert
    if params[:data] == "audit"
      @file_name = "Audits-Report"
      @data = Audit.all.order("id ASC")
    elsif params[:data] == "item"
      @file_name = "Inventory-Report"
      @data = Item.all.order("id ASC")
    elsif params[:data] == "vendor"
      @file_name = "Vendors-Report"
      @data = Vendor.all.order("id ASC")
    elsif params[:data] == "user"
      @file_name = "Users-Report"
      @data = User.all.order("id ASC")
    else
    end



    respond_to do |format|
      format.html
      format.csv { send_data @data.to_csv, filename: "#{@file_name}-#{Date.today}.csv" }
    end
  end

end