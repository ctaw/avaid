class Admin::UsersController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :load_data, :only=> [:new, :edit, :create, :update]

  def index
    @users = User.all.order("email ASC")
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.password = @user.last_name.downcase + @user.email[0...4]

    if params[:warehouse_id].present?
      w_id = params[:warehouse_id].to_i
      unless WarehouseWorker.where(email: @user.email).exists?
        full_name = "#{@user.first_name} #{@user.last_name}"
        warehouse = WarehouseWorker.create(warehouse_id: w_id, name: full_name, email: @user.email)
      end
    end

    if @user.save
      redirect_to admin_users_path, notice: "Successfully Added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if params[:warehouse_id].present?
      w_id = params[:warehouse_id].to_i
      unless WarehouseWorker.where(email: @user.email).exists?
        full_name = "#{@user.first_name} #{@user.last_name}"
        warehouse = WarehouseWorker.create(warehouse_id: w_id, name: full_name, email: @user.email)
      end
    end

    if @user.update(user_params)
      redirect_to admin_users_path, notice: "Successfully Updated"
    else
      render :edit
    end
  end

  private

  def set_id
    @user = User.find(params[:id])
  end

  def load_data
    @roles = Role.all.order(:name)
    @warehouses = Warehouse.all.order(:name)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :is_active, :role_id, :password, :direct_number)
  end

end