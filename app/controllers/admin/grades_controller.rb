class Admin::GradesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @grades = Grade.all.order('name ASC')
  end

  def new
    @grade = Grade.new
  end

  def create
    @grade = Grade.create(grade_params)

    if @grade.save
      redirect_to admin_grades_path, notice: "Grade was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @grade.update(grade_params)
      redirect_to admin_grades_path, notice: "Grade was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @grade = Grade.find(params[:id])
    check_item = Item.where(grade_id: @grade.id).exists?

    unless check_item
      @grade.destroy
      message = "Grade destroyed successfully"
    else
      message = "Grade can't be deleted there are items asssociated to this grade."
    end

    respond_to do |format|
      format.html { redirect_to admin_grades_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @grade = Grade.find(params[:id])
  end

  def grade_params
    params.require(:grade).permit(:name)
  end

end