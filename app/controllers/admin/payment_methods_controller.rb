class Admin::PaymentMethodsController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @payment_methods = PaymentMethod.all.order('name ASC')
  end

  def new
    @payment_method = PaymentMethod.new
  end

  def create
    @payment_method = PaymentMethod.create(payment_method_params)

    if @payment_method.save
      redirect_to admin_payment_methods_path, notice: "Payment Method was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @payment_method.update(payment_method_params)
      redirect_to admin_payment_methods_path, notice: "Payment Method was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @payment_method = PaymentMethod.find(params[:id])
    if @payment_method.destroy
      message = "Payment Method destroyed successfully"
    else
      message = "Payment Method could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_payment_methods_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @payment_method = PaymentMethod.find(params[:id])
  end

  def payment_method_params
    params.require(:payment_method).permit(:name)
  end

end