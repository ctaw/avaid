class Admin::CustomersController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :load_data, :only=> [:new, :edit, :create, :update]

  def index
    @customers = Customer.all.order('name ASC')
  end

  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.create(customers_params)

    if @customer.save
      redirect_to admin_customers_path, notice: "Customer was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @customer.update(customers_params)
      redirect_to admin_customers_path, notice: "Customer was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @customer = Customer.find(params[:id])
    if @customer.destroy
      message = "Customer destroyed successfully"
    else
      message = "Customer could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_customers_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
      tax = Tax.where(country_code: params[:country]).where(province_code: params[:state]).first
      @tax = tax.present? ? tax.id : 0

      @tax_code = tax.present? ? tax.code : "NA"
      @tax_percentage = tax.present? ? tax.percentage : "0%"
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def load_data
    @taxes = Tax.all.order("id DESC")
  end

  def set_id
    @customer = Customer.find(params[:id])
  end

  def customers_params
    params.require(:customer).permit(:name, :address1, :address2, :country_code, :city_code,
      :province_code, :postal_code, :phone_number, :fax_number, :contact_name, :email, :tax_id)
  end

end