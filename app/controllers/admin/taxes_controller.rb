class Admin::TaxesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @taxes = Tax.all.order('id DESC')
  end

  def new
    @tax = Tax.new
  end

  def create
    @tax = Tax.create(tax_params)

    if @tax.save
      redirect_to admin_taxes_path, notice: "Tax was successfully added"
    else
      render :new
    end
  end

  def edit
    @states = CS.states(@tax.country_code.downcase.to_sym)
  end

  def update
    if @tax.update(tax_params)
      redirect_to admin_taxes_path, notice: "Tax ID: #{@tax.id} was successfully added"
    else
      render :edit
    end
  end

  def destroy
    @tax = Tax.find(params[:id])
    if @tax.destroy
      message = "Grade destroyed successfully"
    else
      message = "Grade could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_taxes_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def set_id
    @tax = Tax.find(params[:id])
  end

  def tax_params
    params.require(:tax).permit(:country_code, :province_code, :code, :percentage)
  end

end