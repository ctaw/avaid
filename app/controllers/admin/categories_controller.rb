class Admin::CategoriesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all.order("id DESC")
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to "/admin/categories", notice: "Successfully Added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @category.update(category_params)
      redirect_to "/admin/categories", notice: "Category was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @category = Category.find(params[:id])
    check_item = Item.where(category_id: @category.id).exists?

    unless check_item
      @category.destroy
      message = "Category destroyed successfully"
    else
      message = "Category can't be deleted there are items asssociated to this category."
    end

    respond_to do |format|
      format.html { redirect_to admin_categories_path, :notice => message }
      format.json { head :ok }
    end
  end


  private

  def set_id
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:sku, :name, :description, sub_categories_attributes: [:id, :sku, :name, :_destroy])
  end


end