class Admin::MakersController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @makers = Maker.all.order("id DESC")
    @no_makers = MakerModel.where(maker_id: 0)
  end

  def new
    @maker = Maker.new
  end

  def create
    @maker = Maker.new(maker_params)
    if @maker.save
      redirect_to "/admin/makers", notice: "Successfully Added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @maker.update(maker_params)
      redirect_to "/admin/makers", notice: "Maker was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @maker = Maker.find(params[:id])
    check_item = Item.where(maker_id: @maker.id).exists?

    unless check_item
      @maker.destroy
       message = "Make destroyed successfully"
     else
       message = "Make can't be deleted there are items asssociated to this make."
    end

    respond_to do |format|
      format.html { redirect_to admin_makers_path, :notice => message }
      format.json { head :ok }
    end
  end


  private

  def set_id
    @maker = Maker.find(params[:id])
  end

  def maker_params
    params.require(:maker).permit(:sku, :name, :description, maker_models_attributes: [:id, :sku, :name, :_destroy])
  end


end