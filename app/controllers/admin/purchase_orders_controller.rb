class Admin::PurchaseOrdersController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :load_data, :only=> [:new, :edit, :create, :update]
  before_action :vendor_warehouse_values, :only => [:edit, :new]
  before_action :filter_data, :only => [:index]

  def index
    params.permit!
    if params[:purchase_order].present?
      search = params[:purchase_order].select {|k,v| v != ""}
      purchase_order_params = search.to_h
      @purchase_orders = PurchaseOrder.where(purchase_order_params).order("id DESC") if current_user.role.name == "admin"
      @purchase_orders = PurchaseOrder.where(user_id: current_user.id).where(purchase_order_params).order("id DESC") if current_user.role.name == "sales"
    else
      @purchase_orders = PurchaseOrder.all.order("id DESC") if current_user.role.name == "admin"
      @purchase_orders = PurchaseOrder.where(user_id: current_user.id).order("id DESC") if current_user.role.name == "sales"
    end
  end

  def new
    @purchase_order = PurchaseOrder.new
  end

  def create
    @purchase_order = PurchaseOrder.new(purchase_order_params)

    if @purchase_order.vendor_id == 0 || @purchase_order.vendor_id.nil?
      vendor_params = {
        name: params[:purchase_order][:vendor_id],
        address1: params[:vendor][:address1],
        address2: params[:vendor][:address2],
        country_code: params[:vendor][:country_code],
        province_code: params[:vendor][:province_code],
        city_code: params[:vendor][:city_code],
        postal_code: params[:vendor][:postal_code],
        phone_number: params[:vendor][:phone_number],
        fax_number: params[:vendor][:fax_number],
        contact_person: params[:vendor][:contact_person],
        email: params[:vendor][:email]
      }
      @vendor = Vendor.create!(vendor_params)
      @purchase_order.vendor_id = @vendor.id
    end

    if @purchase_order.warehouse_id == 0
      warehouse_params = {
        name: params[:purchase_order][:warehouse_id],
        address1: params[:warehouse][:address1],
        address2: params[:warehouse][:address2],
        country_code: params[:warehouse][:country_code],
        province_code: params[:warehouse][:province_code],
        city_code: params[:warehouse][:city_code],
        postal_code: params[:warehouse][:postal_code],
        phone_number: params[:warehouse][:phone_number],
        fax_number: params[:warehouse][:fax_number],
        contact_person: params[:warehouse][:contact_person],
        email: params[:warehouse][:email]
      }
      @warehouse = warehouse.create!(warehouse_params)
      @purchase_order.warehouse_id = @warehouse.id
    end

    if @purchase_order.save
      redirect_to admin_purchase_order_path(@purchase_order), notice: "Successfully Added"
    else
      render :new
    end
  end

  def show
    @po_first_id = PurchaseOrder.first
    @po_last_id = PurchaseOrder.last

    @already_sent = Item.where(purchase_order_id: @purchase_order.id).exists?
  end

  def get_prev_item
    @result = PurchaseOrder.where("id < ?", params[:purchase_order_id]).last
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def get_next_item
    @result = PurchaseOrder.where("id > ?", params[:purchase_order_id]).first
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def edit
  end

  def update
    if @purchase_order.vendor_id == 0 || @purchase_order.vendor_id.nil?
      vendor_params = {
        name: params[:purchase_order][:vendor_id],
        address1: params[:vendor][:address1],
        address2: params[:vendor][:address2],
        country: params[:vendor][:country_code],
        province: params[:vendor][:province_code],
        city: params[:vendor][:city_code],
        postal: params[:vendor][:postal_code],
        phone: params[:vendor][:phone_number],
        fax: params[:vendor][:fax_number],
        contact_person: params[:vendor][:contact_person],
        email: params[:vendor][:email]
      }
      @vendor = Vendor.create!(vendor_params)
      @purchase_order.vendor_id = @vendor.id
    end

    if @purchase_order.warehouse_id == 0
      warehouse_params = {
        name: params[:purchase_order][:warehouse_id],
        address1: params[:warehouse][:address1],
        address2: params[:warehouse][:address2],
        country: params[:warehouse][:country_code],
        province: params[:warehouse][:province_code],
        city: params[:warehouse][:city_code],
        postal: params[:warehouse][:postal_code],
        phone: params[:warehouse][:phone_number],
        fax: params[:warehouse][:fax_number],
        contact_person: params[:warehouse][:contact_person],
        email: params[:warehouse][:email]
      }
      @warehouse = warehouse.create!(warehouse_params)
      @purchase_order.warehouse_id = @warehouse.id
    end

    if @purchase_order.update(purchase_order_params)
      redirect_to "/admin/purchase_orders/#{@purchase_order.id}", notice: "Purchase Order was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @purchase_order = PurchaseOrder.find(params[:id])
    if @purchase_order.destroy
      message = "Purchase Order destroyed successfully"
    else
      message = "Purchase Order could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_purchase_orders_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_vendor_details
    vendor_id = params[:vendor_id].to_i
    @vendor = Vendor.find(vendor_id)

    if @vendor.tax_id.present?
      @tax = Tax.where(id: @vendor.tax_id).first
      @data = {
        id: @vendor.id,
        name: @vendor.name,
        address1: @vendor.address1,
        address2: @vendor.address2,
        city_code: @vendor.city_code,
        country_code: @vendor.country_code,
        province_code: @vendor.province_code,
        postal_code: @vendor.postal_code,
        phone_number: @vendor.phone_number,
        fax_number: @vendor.fax_number,
        contact_person: @vendor.contact_name,
        email: @vendor.email,
        tax_id: @vendor.tax_id,
        percentage: @tax.percentage
      }
    else
      @data = {
        id: @vendor.id,
        name: @vendor.name,
        address1: @vendor.address1,
        address2: @vendor.address2,
        city_code: @vendor.city_code,
        country_code: @vendor.country_code,
        province_code: @vendor.province_code,
        postal_code: @vendor.postal_code,
        phone_number: @vendor.phone_number,
        fax_number: @vendor.fax_number,
        contact_person: @vendor.contact_name,
        email: @vendor.email,
        tax_id: @vendor.tax_id,
        percentage: 0
      }
    end

    respond_to do |format|
      format.html {}
      format.json {render json: @data}
    end
  end

  def get_warehouse_details
    warehouse_id = params[:warehouse_id].to_i
    @warehouse = Warehouse.find(warehouse_id)

    respond_to do |format|
      format.html {}
      format.json {render json: @warehouse}
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_tax_details
    if params[:tax_id].present?
      @tax = Tax.find(params[:tax_id])
    end
    respond_to do |format|
      format.json { render json: @tax }
    end
  end

  def clone_with_item
    purchase_order = PurchaseOrder.find(params[:purchase_order_id])
    purchase_order_items = PurchaseOrderItem.where(purchase_order_id: purchase_order.id)

    @clone_purchase_order = purchase_order.dup
    @clone_purchase_order.save!

    purchase_order_items.each do |purchase_order_item|
      po_item = purchase_order_item.dup
      po_item.purchase_order_id = @clone_purchase_order.id
      po_item.save
    end

    respond_to do |format|
      format.json { render json: @clone_purchase_order }
    end
  end

  def clone
    purchase_order = PurchaseOrder.find(params[:purchase_order_id])
    @clone_purchase_order = purchase_order.dup
    @clone_purchase_order.save!

    respond_to do |format|
      format.json { render json: @clone_purchase_order }
    end
  end

  def add_to_inventory
    @po = PurchaseOrder.find(params[:purchase_order_id])  
    @status = Status.where('lower(name) = ?', "in").first
    purchase_order_items = PurchaseOrderItem.where(purchase_order_id: params[:purchase_order_id])

    items = []
    purchase_order_items.each_with_index do |po_item, index|
      quantity = po_item.qty.to_i
      quantity.times do
        items << { purchase_order_id: @po.id, purchase_order_item_sku: po_item.item_sku, description: po_item.description, qty: 1, cost: po_item.rate, vendor_id: @po.vendor_id, status_id: @status.id }
      end
    end
    if items.any?
      Item.create!(items)
    end
  end

  private

  def set_id
    @purchase_order = PurchaseOrder.find(params[:id])
  end

  def load_data
    @vendors = Vendor.all.order('name ASC')
    @warehouses = Warehouse.all.order('name ASC')
    @users = User.all
    @current_users = User.where(id: current_user.id)
    @payment_methods = PaymentMethod.all.order("name")
    @taxes = Tax.all.order("id DESC")
  end

  def vendor_warehouse_values
    @vendor_address1 = @purchase_order.present? ? @purchase_order.vendor.address1 : nil
    @vendor_address2 = @purchase_order.present? ? @purchase_order.vendor.address2 : nil
    @vendor_country = @purchase_order.present? ? @purchase_order.vendor.country_code : nil
    @vendor_province = @purchase_order.present? ? @purchase_order.vendor.province_code : 'Select Country 1st'
    @vendor_city = @purchase_order.present? ? @purchase_order.vendor.city_code : 'Select Province 1st'
    @vendor_postal = @purchase_order.present? ? @purchase_order.vendor.postal_code : nil
    @vendor_phone = @purchase_order.present? ? @purchase_order.vendor.phone_number : nil
    @vendor_fax = @purchase_order.present? ? @purchase_order.vendor.fax_number : nil
    @vendor_contact = @purchase_order.present? ? @purchase_order.vendor.contact_person : nil
    @vendor_email = @purchase_order.present? ? @purchase_order.vendor.email : nil

    @warehouse_address1 = @purchase_order.present? ? @purchase_order.warehouse.address1 : nil
    @warehouse_address2 = @purchase_order.present? ? @purchase_order.warehouse.address2 : nil
    @warehouse_country = @purchase_order.present? ? @purchase_order.warehouse.country_code : nil
    @warehouse_province = @purchase_order.present? ? @purchase_order.warehouse.province_code : 'Select Country 1st'
    @warehouse_city = @purchase_order.present? ? @purchase_order.warehouse.city_code : 'Select Province 1st'
    @warehouse_phone = @purchase_order.present? ? @purchase_order.warehouse.phone_number : nil
    @warehouse_fax = @purchase_order.present? ? @purchase_order.warehouse.fax_number : nil
    @warehouse_contact = @purchase_order.present? ? @purchase_order.warehouse.contact_person : nil
    @warehouse_email = @purchase_order.present? ? @purchase_order.warehouse.email : nil
  end

  def filter_data
    @users = User.all.order("email")
    @vendors = Vendor.all.order("name")
  end

  def purchase_order_params
    params.require(:purchase_order).permit(:vendor_id, :warehouse_id, :purchase_order_number, :purchased_date, :user_id,
      :tax_id, :payment_method_id, :note, :tax_percentage, :sub_total, :total, :freight,
      purchase_order_items_attributes: [:id, :item_sku, :description, :qty, :rate, :amount, :_destroy])
  end


end