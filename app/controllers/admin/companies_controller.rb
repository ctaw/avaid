class Admin::CompaniesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @companies = Company.all.order('name ASC')
  end

  def new
    @company = Company.new
  end

  def create
    @company = Company.create(company_params)

    if @company.save
      redirect_to admin_company_path(@company), notice: "Company was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @company.update(company_params)
      redirect_to "/admin/companies/#{@company.id}", notice: "Company was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @company = Company.find(params[:id])
    if @company.destroy
      message = "Company destroyed successfully"
    else
      message = "Company could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_companies_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def set_id
    @company = Company.find(params[:id])
  end

  def company_params
    params.require(:company).permit(:name, :address1, :address2, :country_code, :city_code,
      :province_code, :postal_code, :phone_number, :fax_number, :contact_person, :email)
  end

end