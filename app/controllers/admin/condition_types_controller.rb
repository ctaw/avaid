class Admin::ConditionTypesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @condition_types = ConditionType.all.order('name ASC')
  end

  def new
    @condition_type = ConditionType.new
  end

  def create
    @condition_type = ConditionType.create(condition_params)

    if @condition_type.save
      redirect_to admin_condition_types_path, notice: "Condition was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @condition_type.update(condition_params)
      redirect_to admin_condition_types_path, notice: "Condition was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @condition_type = ConditionType.find(params[:id])
    if @condition_type.destroy
      message = "Condition destroyed successfully"
    else
      message = "Condition could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_condition_types_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @condition_type = ConditionType.find(params[:id])
  end

  def condition_params
    params.require(:condition_type).permit(:name)
  end

end