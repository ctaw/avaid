class Admin::DeliveryMethodsController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @delivery_methods = DeliveryMethod.all.order('name ASC')
  end

  def new
    @delivery_method = DeliveryMethod.new
  end

  def create
    @delivery_method = DeliveryMethod.create(delivery_method_params)

    if @delivery_method.save
      redirect_to admin_delivery_methods_path, notice: "Delivery Method was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @delivery_method.update(delivery_method_params)
      redirect_to admin_delivery_methods_path, notice: "Delivery Method was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @delivery_method = DeliveryMethod.find(params[:id])
    if @delivery_method.destroy
      message = "Delivery Method destroyed successfully"
    else
      message = "Delivery Method could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_delivery_methods_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @delivery_method = DeliveryMethod.find(params[:id])
  end

  def delivery_method_params
    params.require(:delivery_method).permit(:name)
  end

end