class Admin::CustomerTypesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @customer_types = CustomerType.all.order('name ASC')
  end

  def new
    @customer_type = CustomerType.new
  end

  def create
    @customer_type = CustomerType.create(customer_type_params)

    if @customer_type.save
      redirect_to admin_customer_types_path, notice: "Customer Type was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @customer_type.update(customer_type_params)
      redirect_to admin_customer_types_path, notice: "Customer Type was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @customer_type = CustomerType.find(params[:id])
    if @customer_type.destroy
      message = "Customer Type destroyed successfully"
    else
      message = "Customer Type could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_customer_types_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @customer_type = CustomerType.find(params[:id])
  end

  def customer_type_params
    params.require(:customer_type).permit(:name)
  end

end