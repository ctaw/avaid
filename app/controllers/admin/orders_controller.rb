class Admin::OrdersController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :load_data, :only=> [:new, :create, :edit, :update]

  def index
    @orders = Order.all.order("id DESC") if current_user.role.name == "admin"
    @orders = Order.where(user_id: current_user.id) if current_user.role.name == "sales"
  end

  def show
    @order = Order.find(params[:id])
    @quote = Quote.find(@order.quote_id)

    @shipto = Shipment.where(customer_id: @quote.customer_id).where(quote_id: @quote.id).last

    @order_last_id = Order.last
    @order_first_id = Order.first
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.create(order_params)

    if @order.save
      redirect_to admin_orders_path, notice: "Order was successfully added"
    else
      render :new
    end
  end

  def edit
    shipped_status = OrderStatus.where('lower(name) = ?', "shipped").first
    @is_shipped = @order.order_status_id == shipped_status.id
  end

  def update
    shipped_status = OrderStatus.where('lower(name) = ?', "shipped").first
    cancelled_status = OrderStatus.where('lower(name) = ?', "cancelled").first

    if params[:order][:order_status_id].to_i == shipped_status.id
      if params[:order][:shipped_date].nil? || params[:order][:shipped_date] == ""
        params[:order][:shipped_date] = Time.zone.now
      end

      quote = Quote.find(params[:order][:quote_id])
      quote.update(status: "shipped")

      quote.quote_items.each do |quote_item|
        if quote_item.item_id.present?
          item = Item.find(quote_item.item_id)
          status = Status.where('lower(name) = ?', "out").first
          item.update(status_id: status.id)
        end
      end
    elsif params[:order][:order_status_id].to_i == cancelled_status.id
      quote = Quote.find(params[:order][:quote_id])
      quote.update(status: "quote")
    else
      ""
    end

    if @order.update(order_params)
      # Sends quote email
      if @order.order_status_id == shipped_status.id
        @quoted_order = Order.where(quote_id: @order.quote_id).last
        @salesperson = @order.user_id.present? ? @order.user.email : @quoted_order.user.email
        OrderMailer.send_order(@salesperson, @order).deliver
      end

      if @order.order_items.any?
        @status = Status.where('lower(name) = ?', "assigned").first
        @order.order_items.each do |order_item|
          item = Item.find(order_item.item_id)
          item.update(status_id: @status.id, order_id: @order.id)
        end
      else
        items = Item.where(order_id: @order.id)
        items.each do |item|
          item_o = Item.find(item.id)
          item_o.update(status_id: "in", order_id: nil)
        end
      end

      if OrderItem.where(order_id: @order.id).exists?
        order_item_ids = OrderItem.where(order_id: @order.id).pluck(:item_id)
        item_ids = Item.where(order_id: @order.id).pluck(:id)
        item_del_ids = (order_item_ids-item_ids) | (item_ids-order_item_ids)

        Item.where(id: item_del_ids).update(order_id: nil)

      end
      redirect_to admin_orders_path, notice: "Order was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @quote = Quote.find(@order.quote_id)

    if @order.destroy
      @quote.update(status: "quote")
      message = "Order destroyed successfully and Quote changed to quote status"
    else
      message = "Order can't be deleted"
    end

    respond_to do |format|
      format.html { redirect_to admin_orders_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_quote
    if params[:quote_id].present?
      @quote = Quote.find(params[:quote_id])
    end
    respond_to do |format|
      format.html {}
      format.json {render json: @quote}
    end
  end

  def update_maker_model
    if params[:maker_id].present?
      @maker_models = MakerModel.select("id, name").where(maker_id: params[:maker_id]).order("name ASC")
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def update_items
    if params[:maker_model_id].present?
      @items = Item.where(maker_id: params[:maker_id]).where(maker_model_id: params[:maker_model_id])
    else
      false
    end
    respond_to do |format|
      format.js
    end
  end

  def get_prev_item
    @result = Order.where("id < ?", params[:order_id]).last
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def get_next_item
    @result = Order.where("id > ?", params[:order_id]).first
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def autocomplete
    @result = Item.where("serial like ?", "%#{params[:serial].downcase}%").first
    respond_to do |format|
      format.json { render json: @result }
    end
  end


  private

  def set_id
    @order = Order.find(params[:id])
  end

  def load_data
    @quote_order_ids = Order.all.pluck(:quote_id)
    if current_user.role.name == "admin"
      @available_quotes = Quote.where.not(id: @quote_order_ids).order(:id)
      Quote.where('id NOT IN (?)', @quote_order_ids)
      @users = User.all.order(:email)
    else
      @current_users = User.where(id: current_user.id)
      @available_quotes = Quote.where(user_id: current_user.id).where.not(id: @quote_order_ids).order(:id)
    end

    @statuses = [['Quote','quote'], ['Approved','approved'], ['Shipped','shipped'], ['Cancelled', 'cancelled']]
    @delivery_methods = DeliveryMethod.all.order('name ASC')
    @delivery_types = DeliveryType.all.order('name ASC')
    @items = Item.all.order('id DESC')
    @makes = Maker.all.order(:name)
    @order_statuses = OrderStatus.all.order(:id)
  end

  def order_params
    params.require(:order).permit(:quote_id, :user_id, :delivery_method_id, :delivery_type_id, :shipped_date, :order_status_id, order_items_attributes: [:id, :item_id, :serial, :_destroy])
  end

end