class Admin::ProductWarrantiesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @product_warranties = ProductWarranty.all.order('name ASC')
  end

  def new
    @product_warranty = ProductWarranty.new
  end

  def create
    @product_warranty = ProductWarranty.create(product_warranty_params)

    if @product_warranty.save
      redirect_to admin_product_warranties_path, notice: "Product Warranty was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @product_warranty.update(product_warranty_params)
      redirect_to admin_product_warranties_path, notice: "Product Warranty was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @product_warranty = ProductWarranty.find(params[:id])
    if @product_warranty.destroy
      message = "Product Warranty destroyed successfully"
    else
      message = "Product Warranty could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_product_warranties_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @product_warranty = ProductWarranty.find(params[:id])
  end

  def product_warranty_params
    params.require(:product_warranty).permit(:name)
  end

end