class Admin::DevicesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @devices = Device.all.order('name ASC')
  end

  def new
    @device = Device.new
  end

  def create
    @device = Device.create(device_params)

    if @device.save
      redirect_to admin_device_path(@device), notice: "Device was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @device.update(device_params)
      redirect_to "/admin/device/#{@device.id}", notice: "Device was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @device = Device.find(params[:id])
    if @device.destroy
      message = "Device destroyed successfully"
    else
      message = "Device could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_devices_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @device = Device.find(params[:id])
  end

  def device_params
    params.require(:device).permit(:name)
  end

end