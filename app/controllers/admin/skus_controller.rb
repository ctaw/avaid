class Admin::SkusController < AdminController

  def index
    @skus = Sku.all.order("name")
  end

end