class Admin::ImportDataController < AdminController

  def index
  end

  def create
    if params[:data][:name] == "WipeDrive"
      Item.import_wipe_csv(params[:file])
    elsif params[:data][:name] == "Sku"
      Sku.import_from_csv(params[:file])
    elsif params[:data][:name] == "Blancco"
      Item.import_from_csv(params[:file])
    elsif params[:data][:name] == "Customer"
      Customer.import_from_csv(params[:file])
    elsif params[:data][:name] == "Vendor"
      Vendor.import_from_csv(params[:file])
    else
      return false
    end

    redirect_to '/admin/import_data', notice: "Data imported."
  end

end