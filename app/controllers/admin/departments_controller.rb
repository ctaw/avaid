class Admin::DepartmentsController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @departments = Department.all.order('name ASC')
  end

  def new
    @department = Department.new
  end

  def create
    @department = Department.create(department_params)

    if @department.save
      redirect_to admin_departments_path, notice: "Department was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @department.update(department_params)
      redirect_to admin_departments_path, notice: "Department was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @department = Department.find(params[:id])
    if @department.destroy
      message = "Department destroyed successfully"
    else
      message = "Department could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_departments_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @department = Department.find(params[:id])
  end

  def department_params
    params.require(:department).permit(:name)
  end

end