class Admin::QuotesController < AdminController

  before_action :load_data, :only=> [:index, :new, :edit, :create, :update, :search]
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :customer_values, :only => [:edit, :new]

  def index
    params.permit!
    if params[:quote].present?
      search = params[:quote].select {|k,v| v != ""}
      quote_params = search.to_h
      @quotes = Quote.where(quote_params).order("id DESC") if current_user.role.name == "admin"
      @quotes = Quote.where(user_id: current_user.id).where(quote_params).order("id DESC") if current_user.role.name == "sales"
    else
      @quotes = Quote.all.order("id DESC") if current_user.role.name == "admin"
      @quotes = Quote.where(user_id: current_user.id).order("id DESC") if current_user.role.name == "sales"
    end
    @statuses = [['Quote','quote'], ['Approved','approved'], ['Shipped','shipped'], ['Cancelled', 'cancelled']]
  end

  def new
    @quote = Quote.new
    @statuses = [['Quote','quote'], ['Approved','approved'], ['Shipped','shipped'], ['Cancelled', 'cancelled']]
  end

  def create
    @quote = Quote.new(quote_params)
    if @quote.customer_id == 0 || @quote.customer_id.nil?
      if params[:quote][:customer_id].present?
        customer_params = {
          name: params[:quote][:customer_id],
          address1: params[:customer][:address1],
          address2: params[:customer][:address2],
          country_code: params[:customer][:country_code],
          province_code: params[:customer][:province_code],
          city_code: params[:customer][:city_code],
          postal_code: params[:customer][:postal_code],
          phone_number: params[:customer][:phone_number],
          fax_number: params[:customer][:fax_number],
          contact_name: params[:customer][:contact_name],
          email: params[:customer][:email]
        }
        @customer = Customer.create!(customer_params)
        @quote.customer_id = @customer.id
      end
    end

    if @quote.save
      @quote.quote_items.each do |quote_item|
        if quote_item.item_id.present?
          item = Item.find(quote_item.item_id)
          item.update(quote_id: quote_item.quote_id)
        end
        if quote_item.part_number.present?
          if Item.where('lower(part_number) = ?', quote_item.part_number.downcase.strip).exists?
            item = Item.where('lower(part_number) = ?', quote_item.part_number.downcase.strip).last
            item.update(quote_id: quote_item.quote_id)
            quote_item.update(item_id: item.id)
          end
        end
      end

      if params[:shipment][:address1].present?
        params[:shipment][:country_code] = @quote.customer.country_code if params[:shipment][:country_code] == ""
        params[:shipment][:province_code] = @quote.customer.province_code if params[:shipment][:province_code] == ""
        params[:shipment][:city_code] = @quote.customer.city_code if params[:shipment][:city_code] == ""

        shipment_params = {
          customer_id: @quote.customer_id,
          quote_id: @quote.id,
          address1: params[:shipment][:address1],
          address2: params[:shipment][:address2],
          country_code: params[:shipment][:country_code],
          province_code: params[:shipment][:province_code],
          city_code: params[:shipment][:city_code],
          postal_code: params[:shipment][:postal_code],
          contact_name: params[:shipment][:contact_name],
          email: params[:shipment][:email]
        }
        @shipment = Shipment.create!(shipment_params)
      end

      if @quote.status == "cancelled"
        if Order.where(quote_id: @quote.id).exists?
          order = Order.where(quote_id: @quote.id).last
          order.destroy
        end
      elsif @quote.status == "approved"
        # check order first
        order = Order.where(quote_id: @quote.id).exists?
        unless order
          os = OrderStatus.where('lower(name) = ?', "processing").first
          Order.create(quote_id: @quote.id, user_id: @quote.user_id, order_status_id: os.id)
        end
      elsif @quote.status == "shipped"
        order = Order.where(quote_id: @quote.id).exists?
        os = OrderStatus.where('lower(name) = ?', "shipped").first
        if order
          o = Order.where(quote_id: @quote.id).last
          o.update(order_status_id: os.id, shipped_date: Time.zone.now)
        else
          Order.create(quote_id: @quote.id, user_id: @quote.user_id, order_status_id: os.id, shipped_date: Time.zone.now)
        end
      end

      redirect_to admin_quote_path(@quote), notice: "Successfully Added"
    else
      render :new
    end
  end

  def edit
    @statuses = [['Quote','quote'], ['Approved','approved'], ['Shipped','shipped'], ['Cancelled', 'cancelled']]
  end

  def update
    if @quote.customer_id == 0 || @quote.customer_id.nil?
      customer_params = {
        name: params[:quote][:customer_id],
        address1: params[:customer][:address1],
        address2: params[:customer][:address2],
        country_code: params[:customer][:country_code],
        province_code: params[:customer][:province_code],
        city_code: params[:customer][:city_code],
        postal_code: params[:customer][:postal_code],
        phone_number: params[:customer][:phone_number],
        fax_number: params[:customer][:fax_number],
        contact_name: params[:customer][:contact_name],
        email: params[:customer][:email]
      }
      @customer = Customer.create!(customer_params)
      @quote.customer_id = @customer.id
    end

    if params[:quote][:status] == "cancelled"
      order = Order.where(quote_id: @quote.id).last
      order.destroy
    elsif params[:quote][:status] == "approved"
      # check order first
      order = Order.where(quote_id: @quote.id).exists?
      unless order
        os = OrderStatus.where('lower(name) = ?', "processing").first
        Order.create(quote_id: @quote.id, user_id: @quote.user_id, order_status_id: os.id)
      end
    elsif params[:quote][:status] == "shipped"
      order = Order.where(quote_id: @quote.id).exists?
      os = OrderStatus.where('lower(name) = ?', "shipped").first
      if order
        o = Order.where(quote_id: @quote.id).last
        o.update(order_status_id: os.id, shipped_date: Time.zone.now)
      else
        Order.create(quote_id: @quote.id, user_id: @quote.user_id, order_status_id: os.id, shipped_date: Time.zone.now)
      end
    end

    if @quote.update(quote_params)

      @quote.quote_items.each do |quote_item|
        if quote_item.item_id.present?
          item = Item.find(quote_item.item_id)
          item.update_attributes(quote_id: quote_item.quote_id)
        end

        if quote_item.part_number.present?
          if Item.where('lower(part_number) = ?', quote_item.part_number.downcase.strip).exists?
            item = Item.where('lower(part_number) = ?', quote_item.part_number.downcase.strip).last
            item.update(quote_id: quote_item.quote_id)

            quote_item.update(item_id: item.id)
          end
        end
      end

      if params[:shipment][:address1].present?
        shipment_params = {
          customer_id: @quote.customer_id,
          quote_id: @quote.id,
          address1: params[:shipment][:address1],
          address2: params[:shipment][:address2],
          country_code: params[:shipment][:country_code],
          province_code: params[:shipment][:province_code],
          city_code: params[:shipment][:city_code],
          postal_code: params[:shipment][:postal_code],
          contact_name: params[:shipment][:contact_name],
          email: params[:shipment][:email]
        }
        @shipment = Shipment.create!(shipment_params)
      end

      redirect_to "/admin/quotes/#{@quote.id}", notice: "Quote was successfully updated"
    else
      render :edit
    end
  end

  def show
    @shipment = Shipment.where(quote_id: @quote.id).last
    @salespersons = User.where(role_id: 2)

    @send_logs = SendLog.where(quote_id: @quote.id).order("id DESC")
    @order = Order.where(quote_id: @quote.id).last

    @quote_last_id = Quote.last
    @quote_first_id = Quote.first

  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       render pdf: "PDF_SAMPLE",
  #              template: 'layouts/show.pdf.erb',
  #              layout: 'show.pdf',
  #              show_as_html: params[:debug].present?,
  #       outline: {
  #         outline: true,
  #         outline_depth: 50
  #       },
  #       margin: {
  #         top: 35,
  #         bottom: 35,
  #         left: 35,
  #         right: 35
  #       },
  #       print_media_type: true,
  #       disable_javascript: false
  #     end
  #   end
  end

  def get_customer_details
    customer_id = params[:customer_id].to_i
    @customer = Customer.find(customer_id)

    if @customer.tax_id.present?
      @tax = Tax.where(id: @customer.tax_id).first
      @data = {
        id: @customer.id,
        name: @customer.name,
        address1: @customer.address1,
        address2: @customer.address2,
        city_code: @customer.city_code,
        country_code: @customer.country_code,
        province_code: @customer.province_code,
        postal_code: @customer.postal_code,
        phone_number: @customer.phone_number,
        fax_number: @customer.fax_number,
        contact_name: @customer.contact_name,
        email: @customer.email,
        tax_id: @customer.tax_id,
        percentage: @tax.percentage
      }
    else
      @data = {
        id: @customer.id,
        name: @customer.name,
        address1: @customer.address1,
        address2: @customer.address2,
        city_code: @customer.city_code,
        country_code: @customer.country_code,
        province_code: @customer.province_code,
        postal_code: @customer.postal_code,
        phone_number: @customer.phone_number,
        fax_number: @customer.fax_number,
        contact_name: @customer.contact_name,
        email: @customer.email,
        tax_id: @customer.tax_id,
        percentage: 0
      }
    end

    respond_to do |format|
      format.html {}
      format.json {render json: @data}
    end
  end

  def get_item_details
    if params[:item_id].present?
      @item = Item.find(params[:item_id])
    end

    respond_to do |format|
      format.html {}
      format.json {render json: @item}
    end
  end

  def update_items
    if params[:maker_model_id].present?
      @items = Item.where(maker_model_id: params[:maker_model_id])

    end

    respond_to do |format|
      format.html {}
      format.json {render json: @items}
    end
  end

  def update_models
    if params[:make_id].present?
      @data = MakerModel.where(maker_id: params[:make_id])

    end

    respond_to do |format|
      format.html {}
      format.json {render json: @data}
    end
  end

  # def destroy
  #   @quote = Quote.find(params[:id])
  #   if Shipment.where(quote_id: @quote.id).exists?
  #     @shipment = Shipment.where(quote_id: @quote.id).last
  #     @shipment.destroy
  #   end
  #
  #   if @quote.destroy
  #     message = "Quote destroyed successfully"
  #   else
  #     message = "Quote could not be destroyed"
  #   end
  #   respond_to do |format|
  #     format.html { redirect_to admin_quotes_path, :notice => message }
  #     format.json { head :ok }
  #   end
  # end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_states_customer
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities_customer
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_tax_details
    if params[:province_code].present?
      @tax = Tax.where(province_code: params[:province_code]).last
    end
    respond_to do |format|
      format.json { render json: @tax }
    end
  end

  def send_email
    if params[:customer_email].present? && params[:quote_id]
      @customer_email = params[:customer_email]
      @quote = Quote.find(params[:quote_id])
      @user_email = params[:user_email]

      @emails = params[:emails]
      if @emails.present?
        @emails << @customer_email
      end

      if params[:cc_myself] == 1
        @emails << @user_email
      end

      # Sends quote email
      send_mail = QuoteMailer.send_quote(@customer_email, @quote, @emails, @user_email).deliver

      if send_mail
        # Save send logs
        SendLog.create!(emails: @emails, quote_id: @quote.id)
      end
    end

    respond_to do |format|
      format.json { render json: @customer }
    end
  end

  def approve_quote
    if params[:quote_id].present?
      quote = Quote.find(params[:quote_id])
      order = Order.where(quote_id: quote.id).exists?
      unless order
        os = OrderStatus.where('lower(name) = ?', "processing").first
        Order.create(quote_id: quote.id, user_id: quote.user_id, order_status_id: os.id)
      end

      quote.update(status: "approved")

      @data = order
    end

    respond_to do |format|
      format.json { render json: @data }
    end
  end


  def get_prev_item
    @result = Quote.where("id < ?", params[:quote_id]).last
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def get_next_item
    @result = Quote.where("id > ?", params[:quote_id]).first
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  private

  def set_id
    @quote = Quote.find(params[:id])
  end

  def load_data
    @customers = Customer.all.order(:name)
    @delivery_methods = DeliveryMethod.all.order(:name)
    @delivery_types = DeliveryType.all.order(:name)
    @usersales = User.where(role_id: 2).order(:email)
    @users = User.all.order(:email)
    @current_users = User.where(id: current_user.id)
    @terms = Term.all.order(:name)
    @departments = Department.all.order(:name)
    @customer_types = CustomerType.all.order(:name)

    @warranties = ProductWarranty.all.order(:name)
    @makes = Maker.all.order(:name)
    @maker_models = MakerModel.all.order(:name)
    @items = Item.all.order(:id)
    @grades = Grade.all.order(:name)
    @taxes = Tax.all.order(:id)

  end

  def customer_values
    @customer_address1 = @quote.present? ? @quote.customer.address1 : nil
    @customer_address2 = @quote.present? ? @quote.customer.address2 : nil
    @customer_country = @quote.present? ? @quote.customer.country_code : nil
    @customer_province = @quote.present? ? @quote.customer.province_code : 'Select Country 1st'
    @customer_city = @quote.present? ? @quote.customer.city_code : 'Select Province 1st'
    @customer_postal = @quote.present? ? @quote.customer.postal_code : nil
    @customer_phone = @quote.present? ? @quote.customer.phone_number : nil
    @customer_fax = @quote.present? ? @quote.customer.fax_number : nil
    @customer_contact = @quote.present? ? @quote.customer.contact_name : nil
    @customer_email = @quote.present? ? @quote.customer.email : nil

    if @quote.present?
      @shipment = Shipment.where(quote_id: @quote.id).last
      if @shipment.present?
        @ship_address1 = @shipment.present? ? @shipment.address1 : nil
        @ship_address2 = @shipment.present? ? @shipment.address2 : nil
        @ship_country = @shipment.present? ? @shipment.country_code : nil
        @ship_province = @shipment.present? ? @shipment.province_code : 'Select Country 1st'
        @ship_city = @shipment.present? ? @shipment.city_code : 'Select Province 1st'
        @ship_postal = @shipment.present? ? @shipment.postal_code : nil
        @ship_contact = @shipment.present? ? @shipment.contact_name : nil
        @ship_email = @shipment.present? ? @shipment.email : nil
      end
    end
  end

  def quote_params
    params.require(:quote).permit(:customer_id, :quote_purchase_number,
      :date_required, :user_id, :quote_date, :term_id, :customer_type_id, :status, :tax_percentage, :subtotal,
      :freight, :total, :note, :tax_id, :tax_amount,
      quote_items_attributes: [:id, :warranty_id, :maker_id, :maker_model_id, :part_number, :item_id, :grade_id, :description, :qty, :each_price, :item_subtotal, :_destroy])
  end

end