class Admin::ProductTypesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @product_types = ProductType.all.order('name ASC')
  end

  def new
    @product_type = ProductType.new
  end

  def create
    @product_type = ProductType.create(product_type_params)

    if @product_type.save
      redirect_to admin_product_types_path, notice: "Product Type was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @product_type.update(product_type_params)
      redirect_to admin_product_types_path, notice: "Product Type was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @product_type = ProductType.find(params[:id])
    if @product_type.destroy
      message = "Product Type destroyed successfully"
    else
      message = "Product Type could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_product_types_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @product_type = ProductType.find(params[:id])
  end

  def product_type_params
    params.require(:product_type).permit(:name)
  end

end