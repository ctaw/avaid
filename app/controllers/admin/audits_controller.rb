class Admin::AuditsController < AdminController

  before_action :load_data, :only=> [:index, :new, :edit, :create, :update, :search]
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index

    params.permit!
    if params[:audit].present?
      search = params[:audit].select {|k,v| v != ""}
      audit_params = search.to_h
      @audits = Audit.where(audit_params).order("id DESC") if current_user.role.name == "admin"
      @audits = Audit.where(user_id: current_user.id).where(audit_params).order("id DESC") if current_user.role.name == "sales"
    else
      @audits = Audit.all.order("id DESC") if current_user.role.name == "admin"
      @audits = Audit.where(user_id: current_user.id).order("id DESC") if current_user.role.name == "sales"
    end

    # @audits = Audit.all.order('audited_date DESC')
    # @items = Item.all.count
    #
    # params.permit!
    # if params[:audit].present?
    #   search = params[:audit].select {|k,v| v != ""}
    #   audit_params = search.to_h
    #   @result = Audit.where(audit_params).order("id DESC")
    # end
  end

  def new
    @audit = Audit.new
  end

  def create
    @audit = Audit.create(audit_params)

    if @audit.save

      if @audit.item_ids.any?
        arry_item_ids = @audit.item_ids.map(&:to_i)
        d_items = Item.where(audit_id: @audit.id)
        d_items.each do |d_item|
          d_item.update_attributes(audit_id: nil)
        end

        @items = Item.where(id: arry_item_ids)
        @items.each do |item|
          item.update_attributes(audit_id: @audit.id)
        end
      end

      redirect_to admin_audit_path(@audit), notice: "Audit was successfully added"
    else
      render :new
    end
  end

  def show
    item_ids = @audit.item_ids.map(&:to_i)
    @audited_items = Item.where(id: item_ids)
    @audit_first_id = Audit.first
    @audit_last_id = Audit.last
  end

  def get_prev_item
    @result = Audit.where("id < ?", params[:audit_id]).last

    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def get_next_item
    @result = Audit.where("id > ?", params[:audit_id]).first
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def edit
  end

  def update
    params[:audit][:item_ids].reject(&:empty?)
    params[:audit][:item_ids] = params[:audit][:item_ids].map(&:to_i)
    params[:audit][:item_ids] = params[:audit][:item_ids].drop(1)

    if @audit.update(audit_params)

      if @audit.item_ids.any?
        arry_item_ids = @audit.item_ids.map(&:to_i)
        d_items = Item.where(audit_id: @audit.id)

        d_items.each do |d_item|
          d_item.update_attributes(audit_id: nil)
        end


        @items = Item.where(id: arry_item_ids)
        @items.each do |item|
          item.update_attributes(audit_id: @audit.id)
        end


      end

      redirect_to "/admin/audits/#{@audit.id}", notice: "Audit was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @audit = Audit.find(params[:id])
    @items = Item.where(audit_id: @audit.id)
    @items.update_all(audit_id: nil)
    
    if @audit.destroy
      message = "Audit destroyed successfully"
    else
      message = "Audit could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_audits_path, :notice => message }
      format.json { head :ok }
    end
  end

  def lists
    @audits = Audit.all.order('audited_date DESC')
  end

  def clone
    audit = Audit.find(params[:audit_id])
    @clone_audit = audit.dup
    @clone_audit.code = nil
    @clone_audit.save!

    respond_to do |format|
      format.json { render json: @clone_audit }
    end
  end

  private

  def set_id
    @audit = Audit.find(params[:id])
  end

  def load_data
    @users = User.all.order('id DESC')
    @current_users = User.where(id: current_user.id)
    @items = Item.all.order('id DESC')
    @customers = Customer.all.order("id ASC")
    @workers = WarehouseWorker.all.order("name ASC")
  end

  def audit_params
    params.require(:audit).permit(:code, :note, :user_id, :audited_date,
      :customer_id, :received_date, :received_by, :audit_start, :no_of_items, :receive_note, item_ids: [])
  end

end