class Admin::ItemsController < AdminController
  before_action :load_data, :only=> [:index, :new, :edit, :create, :update, :lists, :editable]
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    params.permit!
    @items = Item.all.order("id ASC")
    @all_items = Item.all.count

    if params[:item].present? || params[:item_category].present?
      search = params[:item].select {|k,v| v != ""}
      search_2 = params[:item_category].select {|k,v| v != ""}

      item_params = search.to_h
      item_category_params = search_2.to_h

      if !item_category_params.empty?
        @item_ids = ItemCategory.where(item_category_params).pluck(:item_id)
        @items = Item.where(id: @item_ids).where(item_params).order("id ASC")
      else
        @items = Item.where(item_params).order("id ASC")
        @item_ids = @items.pluck(:id)
      end
      @result = @items.count
      @last_item = @items.last

      @item_last_id = @item_ids.min
      @item_first_id = @item_ids.max

      if params[:prev].present?
        @last_item = Item.find(params[:prev])
        index = params[:prev].to_i
        sorted = @item_ids.sort_by(&:to_i)
        @item_id_elem = sorted.index(index)
      elsif params[:next].present?
        @last_item = Item.find(params[:next])
        index = params[:next].to_i
        sorted = @item_ids.sort_by(&:to_i)
        @item_id_elem = sorted.index(index)
      end

    elsif params[:item].present? && params[:search].present?

      search = params[:item].select {|k,v| v != ""}
      search_2 = params[:item_category].select {|k,v| v != ""}

      item_params = search.to_h
      item_category_params = search_2.to_h

      if !item_category_params.empty?
        @item_ids = ItemCategory.where(item_category_params).pluck(:item_id)
        @items = Item.where(id: @item_ids).where(item_params).order("id ASC")
      else
        @items = Item.where(item_params).order("id ASC")
        @item_ids = @items.pluck(:id)
      end

      index = params[:search].to_i - 1
      sorted = @item_ids.sort_by(&:to_i)
      id = sorted[index]
      if id.present?
        @last_item = Item.find(id)
      else
        @last_item = @items.last
      end

      @result = @items.count
      @index = @result.present? ? @item_ids.index(@last_item.id) : -1

    else
      @item_ids = @items.pluck(:id)

      @item_last_id = @item_ids.min
      @item_first_id = @item_ids.max

      @item_id_elem = @items.count

      @result = Item.count

      if params[:prev].present?
        @last_item = Item.find(params[:prev])
        index = params[:prev].to_i
        sorted = @item_ids.sort_by(&:to_i)
        @item_id_elem = sorted.index(index)
      elsif params[:next].present?
        @last_item = Item.find(params[:next])
        index = params[:next].to_i
        sorted = @item_ids.sort_by(&:to_i)
        @item_id_elem = sorted.index(index)
      else
        @last_item = Item.last
      end

      if params[:search].present?
        index = params[:search].to_i - 1
        sorted = @item_ids.sort_by(&:to_i)
        id = sorted[index]

        if id.present?
          @last_item = Item.find(id)
        else
          @last_item = Item.last
        end
      end

    end

  end

  def get_prev_item
    @result = Item.where("id < ?", params[:item_id]).last
    if params[:all_params].present?
      search = params[:all_params]
      item_search = JSON.parse search.gsub('=>', ':')
      item_params = item_search.select {|k,v| v != ""}

      @result = Item.where("id < ?", params[:item_id]).where(item_params).last
    end
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def get_next_item
    @result = Item.where("id > ?", params[:item_id]).first
    if params[:all_params].present?
      search = params[:all_params]
      item_search = JSON.parse search.gsub('=>', ':')
      item_params = item_search.select {|k,v| v != ""}

      @result = Item.where("id > ?", params[:item_id]).where(item_params).first
    end
    respond_to do |format|
      format.json { render json: @result }
    end
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)

    maker = Maker.where(id: params[:item][:maker_id]).exists?
    maker_model = MakerModel.where(id: params[:item][:maker_model_id]).exists?

    unless maker
      if params[:item][:maker_id].present?
        @created_make = Maker.create!(name: params[:item][:maker_id])
        @item.maker_id = @created_make.id

        if params[:item][:maker_model_id].present?
          created_model = MakerModel.create!(name: params[:item][:maker_model_id], maker_id: @created_make.id)
          @item.maker_model_id = created_model.id
        else
          @item.maker_model_id = nil
        end

      else
        @item.maker_id = nil
        @item.maker_model_id = nil
      end
    end

    sub_category = SubCategory.where(id: params[:item][:sub_category_id]).exists?

    if params[:item][:category_id].present?
      @cat = Category.find(params[:item][:category_id])

      unless sub_category
        if params[:item][:sub_category_id].present?
          created_sub = SubCategory.create!(name: params[:item][:sub_category_id], category_id: @cat.id)
          @item.sub_category_id = created_sub.id
        else
          @item.sub_category_id = nil
        end
      end

    else
      @item.category_id = nil
      @item.sub_category_id = nil
    end

    warehouse = Warehouse.where(id: params[:item][:warehouse_id]).exists?
    unless warehouse
      if params[:item][:warehouse_id] != ""
        w = Warehouse.create(name: params[:item][:warehouse_id])
        @item.warehouse_id = w.id
      end
    end

    grade = Grade.where(id: params[:item][:grade_id]).exists?
    unless grade
      if params[:item][:grade_id] != ""
        g = Grade.create(name: params[:item][:grade_id])
        @item.grade_id = g.id
      end
    end

    vendor = Vendor.where(id: params[:item][:vendor_id]).exists?
    unless vendor
      if params[:item][:vendor_id] != ""
        v = Vendor.create(name: params[:item][:vendor_id])
        @item.vendor_id = v.id
      end
    end

    if params[:item][:sku] == ""
      if params[:item][:category_id].present? && params[:item][:maker_id].present? && params[:item][:maker_model_id].present?
        cn = Category.find(@item.category_id)
        make = Maker.find(@item.maker_id)
        model = MakerModel.find(@item.maker_model_id)

        if cn.name == "Desktop" || cn.name == "Server" || cn.name == "Laptop"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:processor].delete(" ")
          hdd = params[:item_category][:hard_disk]
          sku_5 = hdd.tr("a-z", "")
          ram = params[:item_category][:ram]
          sku_6 = ram.tr("a-z", "")
          @item.sku = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}-#{sku_6}"
        elsif cn.name == "Portable"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:processor].delete(" ")
          sku_5 = params[:item_category][:ram]
          sku_6 = params[:item_category][:screen_size].delete(" ")
          sku_7 = params[:item_category][:memory_slot].delete(" ")
          params[:item][:sku] = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}-#{sku_6}-#{sku_7}"
        elsif cn.name == "Monitor"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:size].delete(" ")
          sku_5 = params[:item_category][:color].delete(" ")
          @item.sku = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}"
        elsif cn.name == "Printer"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          ram = params[:item_category][:ram]
          sku_4 = ram.tr("a-z", "")
          @item.sku = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}"
        else
          @item.sku = nil
        end
      end
    end

    if @item.save
      if @item.category_id.present?
        @item_category = ItemCategory.new(item_category_params)
        @item_category.item_id = @item.id
        @item_category.category_id = @item.category_id
        @item_category.save
      end

      redirect_to "/admin/items", notice: "Item: #{@item.id} was successfully created."
    else
      render :new
    end

  end

  def edit
    @item_category = ItemCategory.where(item_id: @item.id).where(category_id: @item.category_id).last
    @maker_models = MakerModel.where(maker_id: @item.maker_id)

    if @item.audit_id.present?
      @audit = Audit.find(@item.audit_id)
    else
      @audit = nil
    end

    @models = MakerModel.where(maker_id: @item.maker_id)
    @sub_categories = SubCategory.where(category_id: @item.category_id)
  end

  def update
    # Make and Model
    maker = Maker.where(id: params[:item][:maker_id]).exists?
    maker_model = MakerModel.where(id: params[:item][:maker_model_id]).exists?

    unless maker
      if params[:item][:maker_id].present?
        @created_make = Maker.create!(name: params[:item][:maker_id])
        params[:item][:maker_id] = @created_make.id

        if params[:item][:maker_model_id].present?
          created_model = MakerModel.create!(name: params[:item][:maker_model_id], maker_id: @created_make.id)
          params[:item][:maker_model_id] = created_model.id
        else
          params[:item][:maker_model_id] = nil
        end

      else
        params[:item][:maker_id] = nil
        params[:item][:maker_model_id] = nil
      end
    end

    # Category and Sub Category
    sub_category = SubCategory.where(id: params[:item][:sub_category_id]).exists?

    if params[:item][:category_id].present?
      @cat = Category.find(params[:item][:category_id])

      unless sub_category
        if params[:item][:sub_category_id].present?
          created_sub = SubCategory.create!(name: params[:item][:sub_category_id], category_id: @cat.id)
          params[:item][:sub_category_id] = created_sub.id
        else
          params[:item][:sub_category_id] = nil
        end
      end
    else
      params[:item][:category_id] = nil
      params[:item][:sub_category_id] = nil
    end

    warehouse = Warehouse.where(id: params[:item][:warehouse_id]).exists?
    unless warehouse
      if params[:item][:warehouse_id] != ""
        w = Warehouse.create(name: params[:item][:warehouse_id])
        params[:item][:warehouse_id] = w.id
      end
    end

    grade = Grade.where(id: params[:item][:grade_id]).exists?
    unless grade
      if params[:item][:grade_id] != ""
        g = Grade.create(name: params[:item][:grade_id])
        params[:item][:grade_id] = g.id
      end
    end

    vendor = Vendor.where(id: params[:item][:vendor_id]).exists?
    unless vendor
      if params[:item][:vendor_id] != ""
        v = Vendor.create(name: params[:item][:vendor_id])
        params[:item][:vendor_id] = v.id
      end
    end

    if params[:item][:sku] == ""
      if params[:item][:category_id].present? && params[:item][:maker_id].present? && params[:item][:maker_model_id].present?
        cn = Category.find(params[:item][:category_id])
        make = Maker.find(params[:item][:maker_id])
        model = MakerModel.find(params[:item][:maker_model_id])

        if cn.name == "Desktop" || cn.name == "Server" || cn.name == "Laptop"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:processor].delete(" ")
          hdd = params[:item_category][:hard_disk]
          sku_5 = hdd.tr("a-z", "")
          ram = params[:item_category][:ram]
          sku_6 = ram.tr("a-z", "")
          params[:item][:sku] = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}-#{sku_6}"
        elsif cn.name == "Portable"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:processor].delete(" ")
          sku_5 = params[:item_category][:ram]
          sku_6 = params[:item_category][:screen_size].delete(" ")
          sku_7 = params[:item_category][:memory_slot].delete(" ")
          params[:item][:sku] = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}-#{sku_6}-#{sku_7}"
        elsif cn.name == "Monitor"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          sku_4 = params[:item_category][:size].delete(" ")
          sku_5 = params[:item_category][:color].delete(" ")
          params[:item][:sku] = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}-#{sku_5}"
        elsif cn.name == "Printer"
          sku_1 = cn.name[0,2].upcase
          sku_2 = make.name[0,2].upcase
          sku_3 = model.name.delete(" ")
          ram = params[:item_category][:ram]
          sku_4 = ram.tr("a-z", "")
          params[:item][:sku] = "#{sku_1}-#{sku_2}-#{sku_3}#{sku_4}"
        else
          params[:item][:sku] = nil
        end

      end
    end

    if @item.update(item_params)
      if @item.category_id.present?
        item_category = ItemCategory.where(item_id: @item.id).last

        if item_category.present?
          if item_category.category_id == @item.category_id
            item_category.update(item_category_params)
          else
            delete_item_category = ItemCategory.where(item_id: @item.id).last
            delete_item_category.destroy

            # Create New Item Category with new category
            new_item_category = ItemCategory.new(item_category_params)
            new_item_category.item_id = @item.id
            new_item_category.category_id = @item.category_id
            new_item_category.save
          end
        else
          first_item_category = ItemCategory.new(item_category_params)
          first_item_category.item_id = @item.id
          first_item_category.category_id = @item.category_id
          first_item_category.save
        end
      end

      redirect_to admin_items_path, notice: "Item was successfully updated"
    else
      render :edit
    end
  end

  def show
    @item_category = ItemCategory.where(item_id: @item.id).first

    audits = Audit.all
    array_audit = []
    audits.each do |audit|
      array_audit << audit if audit.item_ids.include?(@item.id.to_s)
    end

    @audited = array_audit
    @item_last_id = Item.last
    @item_first_id = Item.first

    if @audited.any?
      @audit = @audited[0]
    end

    @barcode = barcode_output(@item)

    filename = "Barcode-#{@item.id}"

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "PDF_#{@item.id}",
               template: 'layouts/application.pdf.erb',
               layout: 'item.pdf',
               show_as_html: params[:debug].present?,
        outline: {
          outline: true,
          outline_depth: 50
        },
        margin: {
          top: 35,
          bottom: 35,
          left: 35,
          right: 35
        },
        print_media_type: true,
        disable_javascript: false,
        disposition: 'attachment'
      end
    end

  end

  def barcodes
    params.permit!
    item_ids = params[:item_ids].map(&:to_i)
    @items = Item.where(id: item_ids)

    @barcodes = barcode_multi_output( @items )
    @title = "ID-#{item_ids}-barcodes"

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Barcodes-generate-#{Date.today}",
               template: 'layouts/multiple.pdf.erb',
               layout: 'barcodes.pdf',
               show_as_html: params[:debug].present?,
        outline: {
          outline: true,
          outline_depth: 50
        },
        margin: {
          top: 35,
          bottom: 35,
          left: 35,
          right: 35
        },
        print_media_type: true,
        disable_javascript: false,
        disposition: 'attachment'
      end
    end

  end

  def barcode_multi_output( items )
    barcodes = []
    items.each do |item|
      barcode_string = item.serial || item.id
      barcode = Barby::Code128B.new(barcode_string)
      barcodes << { id: item.id, data: barcode.to_image(height: 25, margin: 5) .to_data_url }
    end
    return barcodes
  end

  def barcode_output( item )
    barcode_string = item.serial || item.id
    barcode = Barby::Code128B.new(barcode_string)

    # PNG OUTPUT
    data = barcode.to_image(height: 25, margin: 5) .to_data_url
  end

  def update_maker_model
    if params[:maker_id].present?
      @maker_models = MakerModel.select("id, name").where(maker_id: params[:maker_id]).order("name ASC")
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def update_sub_category
    if params[:category_id].present?
      @sub_categories = SubCategory.select("id, name").where(category_id: params[:category_id]).order("name ASC")
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def set_device
    @device_name = {}

    if params[:item_id].present?
      @item_category = ItemCategory.where(item_id: params[:item_id].to_i).where(category_id: params[:category_id].to_i).first
      @device = {form: "#{params[:name]}_form", item_device: @item_device}
    else
      @device = {form: "#{params[:name]}_form"}
    end


    respond_to do |format|
      format.js
    end
  end

  def destroy
    audits = Audit.all

    audits.each do |audit|
      if audit.item_ids.include?(@item.id.to_s)
        remove_id = audit.item_ids.delete(@item.id.to_s)
        audit.update_attributes(item_ids: remove_id)
      end
    end

    if @item.destroy
      message = "Item destroyed successfully"
    else
      message = "Item could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_items_path, :notice => message }
      format.json { head :ok }
    end
  end

  def editable
    params.permit!
    if params[:item].present? || params[:item_category].present?

      search = params[:item].select {|k,v| v != ""}
      item_category_params = params[:item_category].select {|k,v| v != ""}
      item_params = search.to_h
      item_ids = ItemCategory.where(item_category_params).pluck(:item_id)
      @items = Item.where(id: item_ids).where(item_params).order("id DESC")
      @result = @items.count


      @barcodes = barcode_multi_output( @items )
    else
      @items = Item.all.order("id DESC")
      @result = @items.count

      @barcodes = barcode_multi_output( @items )
    end

    unless params[:key].nil?
      column = params[:key].to_sym

      item = Item.find(params[:id])
      item.update(column => params[:value])

      if params[:key] == "quote_id"
        quote_item = QuoteItem.where(item_id: params[:id]).first
        quote_item.update(quote_id: params[:value])
      end

      obj = item.as_json
      @data = obj.merge!("column" => params[:key])
    else
      @data = nil
    end

    respond_to do |format|
      format.html
      format.json { render json: @data }
    end
  end

  def lists
    params.permit!
    if params[:item].present? || params[:item_category].present?
      search = params[:item].select {|k,v| v != ""}
      item_category_params = params[:item_category].select {|k,v| v != ""}
      item_params = search.to_h
      item_ids = ItemCategory.where(item_category_params).pluck(:item_id)
      @items = Item.where(id: item_ids).where(item_params).order("id DESC")
      @result = @items.count

      @barcodes = barcode_multi_output( @items )
    else
      @items = Item.all.order("id DESC")
      @result = @items.count

      @barcodes = barcode_multi_output( @items )
    end
  end

  def clone
    item = Item.find(params[:item_id])
    item_category = ItemCategory.where(item_id: item.id).first

    @clone_item = item.dup
    @clone_item.sku = "#{item.sku}-cloned"
    @clone_item.save!

    if item_category.present?
      @clone_item_category = item_category.dup
      @clone_item_category.item_id = @clone_item.id
      @clone_item_category.save
    end

    respond_to do |format|
      format.json { render json: @clone_item }
    end
  end

  def edit_all

    unless params[:data].nil?
      col = params[:data][:edit_item_column]

      if !col.nil? && col != ""
        if col == "maker_id"
          Item.update_all(maker_id: params[:data][:maker_id])
        elsif col == "category_id"
          Item.update_all(category_id: params[:data][:category_id])
        elsif col == "status_id"
          Item.update_all(status_id: params[:data][:status_id])
        elsif col == "condition_type_id"
          Item.update_all(condition_type_id: params[:data][:condition_type_id])
        elsif col == "grade_id"
          Item.update_all(grade_id: params[:data][:grade_id])
        elsif col == "sub_category_id"
          Item.update_all(sub_category_id: params[:data][:sub_category_id])
        elsif col == "maker_model_id"
          Item.update_all(maker_model_id: params[:data][:maker_model_id])
        elsif col == "warehouse_id"
          Item.update_all(warehouse_id: params[:data][:warehouse_id])
        elsif col == "vendor_id"
          Item.update_all(vendor_id: params[:data][:vendor_id])
        elsif col == "part_number"
          Item.update_all(part_number: params[:data][:part_number])
        else
          ""
        end

        redirect_to "/admin/items/editable", notice: "Updated all #{col}"
      else
        redirect_to "/admin/items/editable", notice: "You must have data value"
      end
    end

  end

  def get_subcategories
    category = Category.find(params[:category_id])
    @data = SubCategory.where(category_id: category.id)

    respond_to do |format|
      format.json { render json: @data }
    end
  end

  def get_makermodels
    maker = Maker.find(params[:maker_id])
    @data = MakerModel.where(maker_id: maker.id)

    respond_to do |format|
      format.json { render json: @data }
    end
  end

  private

  def set_id
    @item = Item.find(params[:id])
  end

  def load_data
    @makes = Maker.all.order("name ASC")
    @maker_models = MakerModel.all.order("name ASC")
    @categories = Category.all.order("name ASC")
    @subcategories = SubCategory.all.order("name ASC")
    @condition_types = ConditionType.all.order("id ASC")
    @statuses = Status.all.order("id ASC")
    @assigned_statuses = Status.where(name: "Assigned").order("id ASC")
    @grades = Grade.all.order("id ASC")
    @users = User.all.order("id ASC")
    @vendors = Vendor.all.order('name ASC')
    @warehouses = Warehouse.all.order('name ASC')
    @audits = Audit.select("id, code").all.order("code")

    item_ids = Item.where.not(purchase_order_id: nil).pluck(:purchase_order_id)
    @purchase_orders = PurchaseOrder.where(id: item_ids)
    @pos = PurchaseOrder.all.order("id ASC")

    qitem_ids = Item.where.not(quote_id: nil).pluck(:quote_id)
    @quotes = Quote.where(id: qitem_ids)

  end

  def item_params
    params.require(:item).permit(:status_id, :warehouse_id, :category_id, :sub_category_id, :part_number,
      :maker_id, :maker_model_id, :condition_type_id, :description, :serial, :hdd_serial, :grade, :cost, :cost_of_addons,
      :cost_of_freight, :qty, :grade_id, :vendor_id, :sku, :audit_id, :order_id)
  end

  def item_category_params
    params.require(:item_category).permit(:item_id, :category_id, :bootable, :processor, :speed, :hard_disk, :ram,
      :video_card, :nic, :cd, :coa, :memory_slot, :memory_type, :weight, :cd_drive,
      :screen_size, :screen_type, :modem, :wireless, :floppy, :os, :no_of_hds, :hd_size, :slots,
      :cache, :d_type, :page_count, :size, :manufacture_date, :color, :carrier)
  end

end
