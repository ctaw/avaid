class Admin::OrderStatusesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @order_statuses = OrderStatus.all.order('name ASC')
  end

  def new
    @order_status = OrderStatus.new
  end

  def create
    @order_status = OrderStatus.create(order_status_params)

    if @order_status.save
      redirect_to admin_order_statuses_path, notice: "Order Status was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @order_status.update(order_status_params)
      redirect_to admin_order_statuses_path, notice: "Order Status was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @order_status = OrderStatus.find(params[:id])
    if @order_status.destroy
      message = "Order Status destroyed successfully"
    else
      message = "Order Status could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_order_statuses_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @order_status = OrderStatus.find(params[:id])
  end

  def order_status_params
    params.require(:order_status).permit(:name)
  end

end