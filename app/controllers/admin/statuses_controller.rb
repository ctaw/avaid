class Admin::StatusesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @statuses = Status.all.order('name ASC')
  end

  def new
    @status = Status.new
  end

  def create
    @status = Status.create(status_params)

    if @status.save
      redirect_to admin_statuses_path, notice: "Status was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @status.update(status_params)
      redirect_to admin_statuses_path, notice: "Status was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @status = Status.find(params[:id])
    if @status.destroy
      message = "Status destroyed successfully"
    else
      message = "Status could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_statuses_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @status = Status.find(params[:id])
  end

  def status_params
    params.require(:status).permit(:name)
  end

end