class Admin::WarehousesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @warehouses = Warehouse.all.order("name")
  end

  def new
    @warehouse = Warehouse.new
  end

  def create
    @warehouse = Warehouse.create(warehouse_params)

    if @warehouse.save
      redirect_to admin_warehouses_path, notice: "Warehouse was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @warehouse.update(warehouse_params)
      redirect_to admin_warehouses_path, notice: "Warehouse was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @warehouse = Warehouse.find(params[:id])
    check_item = Item.where(warehouse_id: @warehouse.id).exists?

    unless check_item
      @warehouse.destroy
      message = "Warehouse destroyed successfully"
    else
      message = "Warehouse can't be deleted there are items asssociated to this warehouse."
    end

    respond_to do |format|
      format.html { redirect_to admin_warehouses_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)
    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def set_id
    @warehouse = Warehouse.find(params[:id])
  end

  def warehouse_params
    params.require(:warehouse).permit(:name, :address1, :address2, :country_code, :city_code,
      :province_code, :postal_code, :phone_number, :fax_number, :contact_person, :email, warehouse_workers_attributes: [:id, :name, :email, :_destroy])
  end

end