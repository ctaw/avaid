class Admin::VendorsController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]
  before_action :load_data, :only=> [:new, :edit, :create, :update]

  def index
    @vendors = Vendor.all.order('name ASC')
  end

  def new
    @vendor = Vendor.new
  end

  def create
    @vendor = Vendor.create(vendor_params)

    if @vendor.save
      redirect_to admin_vendors_path, notice: "Vendor was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @vendor.update(vendor_params)
      redirect_to admin_vendors_path, notice: "Vendor was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @vendor = Vendor.find(params[:id])
    check_item = Item.where(vendor_id: @vendor.id).exists?

    unless check_item
      @vendor.destroy
      message = "Vendor destroyed successfully"
    else
      message = "Vendor can't be deleted there are items asssociated to this vendor."
    end

    respond_to do |format|
      format.html { redirect_to admin_vendors_path, :notice => message }
      format.json { head :ok }
    end
  end

  def get_states
    if params[:country].present?
      c = params[:country].parameterize.underscore.to_sym
      @states = CS.states(c)

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  def get_cities
    if params[:state].present?
      c = params[:country].parameterize.underscore.to_sym
      s = params[:state].parameterize.underscore.to_sym
      @cities = CS.get(c, s)

      tax = Tax.where(country_code: params[:country]).where(province_code: params[:state]).first
      @tax = tax.present? ? tax.id : 0

      @tax_code = tax.present? ? tax.code : "NA"
      @tax_percentage = tax.present? ? tax.percentage : "0%"

    else
      false
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def load_data
    @taxes = Tax.all.order("id DESC")
  end

  def set_id
    @vendor = Vendor.find(params[:id])
  end

  def vendor_params
    params.require(:vendor).permit(:name, :address1, :address2, :country_code, :city_code,
      :province_code, :postal_code, :phone_number, :fax_number, :contact_person, :email, :tax_id)
  end

end