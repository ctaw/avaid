class Admin::TermsController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @terms = Term.all.order('name ASC')
  end

  def new
    @term = Term.new
  end

  def create
    @term = Term.create(term_params)

    if @term.save
      redirect_to admin_terms_path, notice: "Term was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @term.update(term_params)
      redirect_to admin_terms_path, notice: "Term was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @term = Term.find(params[:id])
    if @term.destroy
      message = "Term destroyed successfully"
    else
      message = "Term could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_terms_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @term = Term.find(params[:id])
  end

  def term_params
    params.require(:term).permit(:name)
  end

end