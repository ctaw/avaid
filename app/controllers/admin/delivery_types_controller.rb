class Admin::DeliveryTypesController < AdminController
  before_action :set_id, :only=> [:show, :edit, :update, :destroy]

  def index
    @delivery_types = DeliveryType.all.order('name ASC')
  end

  def new
    @delivery_type = DeliveryType.new
  end

  def create
    @delivery_type = DeliveryType.create(delivery_type_params)

    if @delivery_type.save
      redirect_to admin_delivery_types_path, notice: "Delivery Type was successfully added"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @delivery_type.update(delivery_type_params)
      redirect_to admin_delivery_types_path, notice: "Delivery Type was successfully updated"
    else
      render :edit
    end
  end

  def destroy
    @delivery_type = DeliveryType.find(params[:id])
    if @delivery_type.destroy
      message = "Delivery Type destroyed successfully"
    else
      message = "Delivery Type could not be destroyed"
    end
    respond_to do |format|
      format.html { redirect_to admin_delivery_types_path, :notice => message }
      format.json { head :ok }
    end
  end

  private

  def set_id
    @delivery_type = DeliveryType.find(params[:id])
  end

  def delivery_type_params
    params.require(:delivery_type).permit(:name)
  end

end