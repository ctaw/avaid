class Admin::DashboardController < AdminController

  before_action :load_data

  def index
    @year_total_open_quotes_count = Quote.where(user_id: current_user.id).where(status: "quote").where("created_at > ? AND created_at < ?", Time.now.beginning_of_year, Time.now).count
    @year_total_open_quotes = Quote.where(user_id: current_user.id).where(status: "quote").where("created_at > ? AND created_at < ?", Time.now.beginning_of_year, Time.now).sum(:total)
    @month_total_open_quotes_count = Quote.where(user_id: current_user.id).where(status: "quote").where("created_at > ? AND created_at < ?",  Time.now.beginning_of_month, Time.now.end_of_month).count
    @month_total_open_quotes = Quote.where(user_id: current_user.id).where(status: "quote").where("created_at > ? AND created_at < ?",  Time.now.beginning_of_month, Time.now.end_of_month).sum(:total)

    @year_total_approved_quotes_count = Quote.where(user_id: current_user.id).where(status: "approved").where("created_at > ? AND created_at < ?", Time.now.beginning_of_year, Time.now).count
    @year_total_approved_quotes = Quote.where(user_id: current_user.id).where(status: "approved").where("created_at > ? AND created_at < ?", Time.now.beginning_of_year, Time.now).sum(:total)
    @month_total_approved_quotes_count = Quote.where(user_id: current_user.id).where(status: "approved").where("created_at > ? AND created_at < ?",  Time.now.beginning_of_month, Time.now.end_of_month).count
    @month_total_approved_quotes = Quote.where(user_id: current_user.id).where(status: "approved").where("created_at > ? AND created_at < ?",  Time.now.beginning_of_month, Time.now.end_of_month).sum(:total)

    shipped_status = OrderStatus.where('lower(name) = ?', "shipped").first
    open_status = OrderStatus.where('lower(name) = ?', "processing").first

    @year_total_shipped_orders_count = Order.where(user_id: current_user.id).where(order_status_id: shipped_status.id).where("created_at > ? AND created_at < ?", Time.now.beginning_of_year, Time.now).count
    @year_total_shipped_orders = Quote.joins(:orders).where("orders.user_id =?", current_user.id).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date > ? AND orders.shipped_date < ?",  Time.now.beginning_of_month, Time.now.end_of_month).sum(:total)
    @month_total_shipped_orders_count = Order.where(user_id: current_user.id).where(order_status_id: shipped_status.id).where("shipped_date > ? AND shipped_date < ?",  Time.now.beginning_of_month, Time.now.end_of_month).count
    @month_total_shipped_orders = Quote.joins(:orders).where("orders.user_id =?", current_user.id).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date > ? AND orders.shipped_date < ?",  Time.now.beginning_of_month, Time.now.end_of_month).sum(:total)

    # ###################################
    @sales_by_salesperson = []
    @start_date = Time.zone.now
    @end_date = Time.zone.now

    if params[:sales_general].present?
      if params[:sales_general][:filter] == "1"
        @res = Quote.joins(:orders).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date > ? AND orders.shipped_date < ?",  Time.now.beginning_of_month, Time.now.end_of_month)
      elsif params[:sales_general][:filter] == "2"
        @res = Quote.joins(:orders).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date > ? AND orders.shipped_date < ?",  Time.now.beginning_of_month, Time.now.end_of_month)
      elsif params[:sales_general][:filter] == "3"
        @res = Quote.joins(:orders).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date >= ?", 1.week.ago)
      else
        @res = Quote.joins(:orders).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date BETWEEN ? AND ?", @start_date.beginning_of_day, @end_date.end_of_day)
      end

      @res.each do |res|
        if res.user_id.present?
          @sales_by_salesperson << [res.user.email, res.total.to_f]
        end
      end

    else
      @res = Quote.joins(:orders).where("orders.order_status_id =?", shipped_status.id).where("orders.shipped_date BETWEEN ? AND ?", @start_date.beginning_of_day, @end_date.end_of_day)
      @res.each do |res|
        if res.user_id.present?
          @sales_by_salesperson << [res.user.email, res.total.to_f]
        end
      end
    end
    # ###################################


    @year = Date.today.year
    @status_labels = { "In" => "1", "Out" => "2", "Picked" => "3", "Internal" => "4", "Defective" => "5", "Unknown" => "6" }
    @category_labels = { "Laptop" => "1", "Desktop" => "2", "Server" => "3", "Portable" => "4", "Monitor" => "5", "Printer" => "6" }

    if params[:item_by_status].present?
      @status_id = params[:item_by_status][:status_id].to_i
      @item_by_status = Item.where("EXTRACT(year FROM created_at) =?", @year).group(:status_id).where(status_id: params[:item_by_status][:status_id].to_i).count.map {|k,v| [@status_labels.key(k), v] }.to_h
    else
      @status_id = nil
      @item_by_status = Item.where("EXTRACT(year FROM created_at) =?", @year).group(:status_id).count.map {|k,v| [@status_labels.key(k), v] }.to_h
    end

    if params[:item_by_category].present?
      @category_id = params[:item_by_category][:category_id].to_i
      item_categories = Item.where("EXTRACT(year FROM created_at) =?", @year).where(category_id: @category_id).group(:category_id).count
      @item_by_category = item_categories.except!(nil).map {|k,v| [@category_labels.key(k.to_s), v] }.to_h
    else
      @category_id = nil
      item_categories = Item.where("EXTRACT(year FROM created_at) =?", @year).group(:category_id).count
      @item_by_category = item_categories.except!(nil).map {|k,v| [@category_labels.key(k.to_s), v] }.to_h
    end

    if params[:item].present?
      @category_id = params[:item][:category_id].to_i
      @items = Item.where(category_id: @category_id).where('created_at > ?', 30.days.ago)
    else
      @items = Item.all.order(:created_at).where('created_at > ?', 30.days.ago)
    end

  end

  private

  def load_data
    @statuses = Status.all.order("id ASC")
    @categories = Category.all.order("id DESC")
  end

end