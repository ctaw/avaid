module ApplicationHelper

  def parent_child_path?(*test_paths)
    return 'active' if test_paths.include?("/#{request.path.split('/')[2]}")
    ''
  end

  def expanded_path?(*test_paths)
    return 'true' if test_paths.include?("/#{request.path.split('/')[2]}")
    'false'
  end

  def is_expanded?(*test_paths)
    return 'in' if test_paths.include?("/#{request.path.split('/')[2]}")
    ''
  end

  def format_full_date(date)
    return date.blank? ? "" : date.strftime("%b %d, %Y")
  end

  def price_display(number)
    number_to_currency(number, :unit => "$ ")
  end

  def wicked_pdf_image_tag_for_public(img, options={})
    if img[0] == "/"
      new_image = img.slice(1..-1)
      image_tag "file://#{Rails.root.join('public', new_image)}", options
    else
        image_tag img
    end
  end

end
