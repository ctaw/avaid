class OrderMailer < ApplicationMailer

  def send_order(salesperson, order)
    @email = salesperson
    @order = order

    mail(:subject => 'Avaid Order is Shipped', :to => @email)

    return true

  end

end
