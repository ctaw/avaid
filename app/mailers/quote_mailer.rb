class QuoteMailer < ApplicationMailer

  def send_quote(customer_email, quote, emails, user_email)
    @customer_email = customer_email
    @quote = quote
    @user_email = user_email

    mail(:subject => 'Avaid Quote', :to => @customer_email, :cc => emails) do |format|
      format.html
      format.pdf do
        attachments["#{@quote.id}-quote"] = WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => "quote",:template => '/admin/quotes/show.pdf.erb')
        )
      end
    end

    return true

  end

end
