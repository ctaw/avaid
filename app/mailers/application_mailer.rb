class ApplicationMailer < ActionMailer::Base
  default from: '"Avaid" <tawcharlene@gmail.com>'
  layout 'mailer'
end
