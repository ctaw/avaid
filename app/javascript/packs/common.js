$(document).ready(function() {

  $('.datepicker').datepicker({
    dateFormat: 'dd-mm-YYYY'
  });


  // Auto close error message
  setTimeout(function() {
    $("#error-msg").alert('close');
  }, 5000);

  $('.select2').select2({
    placeholder: "Select",
    allowClear: true
  });

  $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-info").slideUp(500);
  });

  $(".upper").bind('keyup', function (e) {
    if (e.which >= 97 && e.which <= 122) {
        var newKey = e.which - 32;
        // I have tried setting those
        e.keyCode = newKey;
        e.charCode = newKey;
    }
    $(this).val($(this).val().toUpperCase());
  });

});
