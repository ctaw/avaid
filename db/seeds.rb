# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Role.create!(name: "admin", description: "Admin User")
Role.create!(name: "sales", description: "SalesPerson")
Role.create!(name: "manager", description: "Manager")
Role.create!(name: "warehouse", description: "Workers")

user = User.create!(email: "charlene@avaid.com", password: "admin123", role_id: 1)
user = User.create!(email: "sales@avaid.com", password: "sales123", role_id: 2)

# condition_types
ConditionType.create([
  {name: "New"},
  {name: "Used"},
  {name: "New Open Box"}
])

Category.create([
  {name: "Laptop", description: "Laptop Fields"},
  {name: "Desktop", description: "Desktop Fields"},
  {name: "Server", description: "Server Fields"},
  {name: "Portable", description: "Portable Fields"},
  {name: "Monitor", description: "Monitor Fields"},
  {name: "Printer", description: "Printer Fields"}
])

SubCategory.create([
    {category_id: 1, name: "Notebook"},
    {category_id: 2, name: "Low Profile"},
    {category_id: 2, name: "Mini Tower"},
    {category_id: 2, name: "All In One"},
    {category_id: 2, name: "Space-saving"},
    {category_id: 2, name: "Desktop"},
    {category_id: 2, name: "Desktop SFF"},
    {category_id: 2, name: "Desktop USFF"},
    {category_id: 2, name: "Desktop Mini"},
    {category_id: 2, name: "Lunch Box"},
    {category_id: 3, name: "Rack Mount Chassis"},
    {category_id: 3, name: "Main Server Chassis"},
    {category_id: 4, name: "Tablet"},
    {category_id: 4, name: "Phone"}
  ])


Status.create([
  {name: "In"},
  {name: "Out"},
  {name: "Picked"},
  {name: "Internal"},
  {name: "Defective"},
  {name: "Unknown"},
  {name: "Assigned"}
])

OrderStatus.create([
  {name: "Processing"},
  {name: "Shipped"},
  {name: "Cancelled"}
])

CustomerType.create([
  {name: "VAR"},
  {name: "CORP"},
  {name: "EDU"},
  {name: "EBAY"},
  {name: "RETAIL"}
])

Term.create([
  {name: "COD"},
  {name: "POD"},
  {name: "Credit Card"},
  {name: "PayPal"},
  {name: "Net 1"},
  {name: "Net 7"},
  {name: "Net 15"},
  {name: "Net 30"},
  {name: "Net 60"}
])

Department.create([
  {name: "Sales"},
  {name: "eBay"}
])

ProductWarranty.create([
  {name: "As Is"},
  {name: "30 Days"},
  {name: "90 Days"},
  {name: "1 Year"},
  {name: "3 Years"},
  {name: "RFT-LTW"}
])

ProductType.create([
  {name: "A"},
  {name: "B"},
  {name: "B1"},
  {name: "C"}
])

DeliveryMethod.create!(name: 'RFT')
DeliveryType.create!(name: 'Ground')


Warehouse.create(name: "Avaid", address1: "250 Road East", address2: "Unit 2", city_code: "Markham", country_code: "CA", province_code: "ON", postal_code: "L3R 1G2", phone_number: "9054708444", fax_number: "9054708444", contact_person: "Office", email: "")

PaymentMethod.create([{name: "Cash"}, {name: "Credit Card"}])

Tax.create([
    {country_code: "CA", province_code: "ON", code: "HST", percentage: "13.0"},
    {country_code: "CA", province_code: "QC", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "AB", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "BC", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "PE", code: "HST", percentage: "14.0"},
    {country_code: "CA", province_code: "SK", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "MB", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "NS", code: "HST", percentage: "15.0"},
    {country_code: "CA", province_code: "NB", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "YT", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "NT", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "NU", code: "GST", percentage: "5.0"},
    {country_code: "CA", province_code: "NL", code: "GST", percentage: "13.0"}
  ])

Maker.create([
  {name: "Lenovo", description: "ThinkPad, ThinkBook, IdeaPad, Yoga, Lenovo Legion"},
  {name: "HP", description: "Envy, Spectre, Elitebook, Pavilion, Omen, Pavilion Gaming"},
  {name: "Dell", description: "Alienware, Inspiron, Latitude, XPS, G Series, Precision"},
  {name: "Asus", description: "ZenBook, Vivo, Republic of Gamers (ROG), TUF Gaming"},
  {name: "Acer", description: "Packard Bell, Predator, Aspire, TravelMate, Nitro"},
  {name: "Apple", description: "MacBook"}
])

lenovo = Maker.where(name: "Lenovo").pluck(:id).first
hp = Maker.where(name: "HP").pluck(:id).first
dell = Maker.where(name: "Dell").pluck(:id).first
asus = Maker.where(name: "Asus").pluck(:id).first
acer = Maker.where(name: "Acer").pluck(:id).first
apple = Maker.where(name: "Apple").pluck(:id).first


MakerModel.create([
    {maker_id: lenovo, name: "T480"},
    {maker_id: lenovo, name: "X1 Yoga"},
    {maker_id: lenovo, name: "P1"},
    {maker_id: lenovo, name: "E580"},
    {maker_id: lenovo, name: "X1 Carbon (7th Gen)"},
    {maker_id: lenovo, name: "X1 Yoga (4th Gen)"},
    {maker_id: lenovo, name: "X1 Extreme (Gen 2)"},
    {maker_id: lenovo, name: "730s"},
    {maker_id: lenovo, name: "720s"},
    {maker_id: lenovo, name: "710s Plus"},
    {maker_id: lenovo, name: "700"},
    {maker_id: lenovo, name: "Y730"},
    {maker_id: lenovo, name: "Y530"},
    {maker_id: lenovo, name: "Y545"},
    {maker_id: lenovo, name: "Y540"},

    {maker_id: hp, name: "Spectre x360"},
    {maker_id: hp, name: "Spectre Folio"},
    {maker_id: hp, name: "Chromebook"},
    {maker_id: hp, name: "Omen"},
    {maker_id: hp, name: "Omen X"},
    {maker_id: hp, name: "Envy x360"},
    {maker_id: hp, name: "Elite Dragonfly"},

    {maker_id: dell, name: "XPS"},
    {maker_id: dell, name: "G7"},
    {maker_id: dell, name: "5580"},
    {maker_id: dell, name: "I5481-5076GRY"},

    {maker_id: asus, name: "UX333FA"},
    {maker_id: asus, name: "UX433"},
    {maker_id: asus, name: "C434"},
    {maker_id: asus, name: "W700G3T"},
    {maker_id: asus, name: "B9450"},

    {maker_id: acer, name: "P614"},
    {maker_id: acer, name: "315-1H"},
    {maker_id: acer, name: "Spin 3"},
    {maker_id: acer, name: "Triton 500"},
    {maker_id: acer, name: "Swift 7"},

    {maker_id: apple, name: "Air"},
    {maker_id: apple, name: "Pro 13"},
    {maker_id: apple, name: "Pro 16"}
  ])


Grade.create([
  { name: "A" },
  { name: "B" },
  { name: "C" },
  { name: "D" },
  { name: "E" }
])


# random_status = Status.all.pluck(:id)
# random_category = Category.all.pluck(:id)
# random_make = Maker.all.pluck(:id)
# random_condition = ConditionType.all.pluck(:id)
# random_grade = Grade.all.pluck(:id)
# random_vendor = Vendor.all.pluck(:id)
# random_customer = [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15]

# 30.times do
#   Item.create(
#     status_id: random_status.sample,
#     warehouse_id: 1,
#     category_id: random_category.sample,
#     part_number: Faker::Alphanumeric.alphanumeric(number: 8),
#     maker_id: random_make.sample,
#     condition_type_id: random_condition.sample,
#     description: Faker::Lorem.sentence,
#     serial: Faker::Alphanumeric.alphanumeric(number: 8),
#     hdd_serial: Faker::Alphanumeric.alphanumeric(number: 8),
#     grade_id: random_grade.sample,
#     vendor_id: random_vendor.sample,
#     qty: 1,
#     cost: Faker::Number.decimal(l_digits: 3, r_digits: 2)
#   )
# end

# 10.times do
#   WarehouseWorker.create(
#     warehouse_id: 1,
#     name: Faker::Name.name,
#     email: Faker::Internet.email
#   )
# end

# random_workers = WarehouseWorker.all.pluck(:name)
#
# 10.times do
#   Audit.create(
#     user_id: 1,
#     note: Faker::Lorem.sentence,
#     receive_note: Faker::Lorem.sentence,
#     customer_id: random_customer.sample,
#     audited_date: Faker::Date.between(from: 40.days.ago, to: Date.today),
#     received_date: Faker::Date.between(from: 40.days.ago, to: Date.today),
#     audit_start: Faker::Date.between(from: 40.days.ago, to: Date.today),
#     no_of_items: Faker::Number.between(from: 1, to: 100),
#     received_by: random_workers.sample
#   )
# end
