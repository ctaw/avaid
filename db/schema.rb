# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_09_095941) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "audits", force: :cascade do |t|
    t.text "note"
    t.integer "user_id"
    t.datetime "audited_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "code"
    t.text "item_ids", default: [], array: true
    t.integer "customer_id"
    t.datetime "audit_date"
    t.datetime "received_date"
    t.datetime "audit_start"
    t.integer "no_of_items"
    t.string "received_by"
    t.text "receive_note"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "condition_types", force: :cascade do |t|
    t.string "name"
  end

  create_table "customer_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.text "address1"
    t.text "address2"
    t.string "city_code"
    t.string "country_code"
    t.string "province_code"
    t.string "postal_code"
    t.string "phone_number"
    t.string "fax_number"
    t.string "contact_name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "tax_id"
  end

  create_table "delivery_methods", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delivery_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "grades", force: :cascade do |t|
    t.string "name"
  end

  create_table "item_categories", force: :cascade do |t|
    t.integer "item_id"
    t.integer "category_id"
    t.boolean "bootable"
    t.string "processor"
    t.string "speed"
    t.string "hard_disk"
    t.string "ram"
    t.string "video_card"
    t.string "nic"
    t.string "cd"
    t.string "coa"
    t.string "memory_slot"
    t.string "memory_type"
    t.string "weight"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "cd_drive"
    t.string "screen_size"
    t.string "screen_type"
    t.string "modem"
    t.string "wireless"
    t.string "floppy"
    t.string "os"
    t.string "no_of_hds"
    t.string "hd_size"
    t.string "cache"
    t.string "d_type"
    t.integer "page_count"
    t.string "size"
    t.datetime "manufacture_date"
    t.string "color"
    t.string "slots"
    t.string "carrier"
  end

  create_table "item_ebays", force: :cascade do |t|
    t.integer "item_id"
    t.decimal "lbs"
    t.decimal "lenght"
    t.decimal "width"
    t.decimal "height"
    t.string "country_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "items", force: :cascade do |t|
    t.string "status_id"
    t.integer "warehouse_id"
    t.integer "category_id"
    t.integer "sub_category_id"
    t.string "part_number"
    t.integer "maker_id"
    t.integer "maker_model_id"
    t.string "condition_type_id"
    t.text "description"
    t.string "serial"
    t.string "hdd_serial"
    t.string "grade"
    t.string "cost"
    t.string "cost_of_addons"
    t.string "cost_of_freight"
    t.integer "qty"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "grade_id"
    t.integer "vendor_id"
    t.string "sku"
    t.integer "purchase_order_id"
    t.string "purchase_order_item_sku"
    t.integer "quote_id"
    t.integer "audit_id"
    t.integer "order_id"
  end

  create_table "maker_models", force: :cascade do |t|
    t.integer "maker_id"
    t.string "name"
  end

  create_table "makers", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "item_id"
    t.integer "order_id"
    t.string "serial"
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string "name"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "quote_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.datetime "shipped_date"
    t.string "status"
    t.integer "delivery_method_id"
    t.integer "delivery_type_id"
    t.text "item_ids", default: [], array: true
    t.integer "order_status_id"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "name"
  end

  create_table "product_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_warranties", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "purchase_order_items", force: :cascade do |t|
    t.integer "purchase_order_id"
    t.string "item_sku"
    t.text "description"
    t.integer "qty"
    t.decimal "rate"
    t.decimal "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "purchase_orders", force: :cascade do |t|
    t.integer "vendor_id"
    t.integer "company_id"
    t.string "purchase_order_number"
    t.datetime "purchased_date"
    t.integer "user_id"
    t.decimal "bulk_weight"
    t.integer "payment_method_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "tax_id"
    t.integer "warehouse_id"
    t.text "note"
    t.text "tax_percentage"
    t.decimal "sub_total"
    t.decimal "total"
    t.decimal "freight"
    t.decimal "tax_amount"
  end

  create_table "quote_items", force: :cascade do |t|
    t.integer "warranty_id"
    t.integer "maker_id"
    t.integer "item_id"
    t.text "description"
    t.integer "qty"
    t.decimal "each_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "quote_id"
    t.decimal "item_subtotal"
    t.integer "grade_id"
    t.integer "maker_model_id"
    t.string "part_number"
  end

  create_table "quotes", force: :cascade do |t|
    t.integer "customer_id"
    t.string "quote_purchase_number"
    t.datetime "date_required"
    t.integer "user_id"
    t.datetime "quote_date"
    t.integer "term_id"
    t.integer "customer_type_id"
    t.boolean "quote_only"
    t.boolean "is_rma"
    t.boolean "is_closed"
    t.boolean "is_shipped"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "freight"
    t.decimal "total"
    t.decimal "subtotal"
    t.text "note"
    t.integer "tax_id"
    t.decimal "tax_percentage"
    t.decimal "tax_amount"
    t.string "status"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "send_logs", force: :cascade do |t|
    t.text "emails"
    t.integer "quote_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "shipments", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "quote_id"
    t.text "address1"
    t.text "address2"
    t.string "city_code"
    t.string "country_code"
    t.string "province_code"
    t.string "postal_code"
    t.string "email"
    t.string "contact_name"
    t.integer "warehouse_id"
  end

  create_table "skus", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name"
  end

  create_table "sub_categories", force: :cascade do |t|
    t.integer "category_id"
    t.string "name"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "code"
    t.decimal "percentage"
    t.string "province_code"
    t.string "country_code"
  end

  create_table "terms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role_id"
    t.string "first_name"
    t.string "last_name"
    t.boolean "is_active"
    t.string "direct_number"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.text "address1"
    t.text "address2"
    t.string "city_code"
    t.string "country_code"
    t.string "province_code"
    t.string "postal_code"
    t.string "phone_number"
    t.string "fax_number"
    t.string "contact_name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "contact_person"
    t.integer "tax_id"
  end

  create_table "warehouse_workers", force: :cascade do |t|
    t.integer "warehouse_id"
    t.string "name"
    t.string "email"
  end

  create_table "warehouses", force: :cascade do |t|
    t.string "name"
    t.text "address1"
    t.text "address2"
    t.string "country_code"
    t.string "province_code"
    t.string "city_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "postal_code"
    t.string "phone_number"
    t.string "fax_number"
    t.string "contact_person"
    t.string "email"
  end

end
