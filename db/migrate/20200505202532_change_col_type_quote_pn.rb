class ChangeColTypeQuotePn < ActiveRecord::Migration[6.0]
  def change
    change_column :quotes, :quote_purchase_number, :string
  end
end
