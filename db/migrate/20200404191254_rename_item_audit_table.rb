class RenameItemAuditTable < ActiveRecord::Migration[6.0]
  def change
    rename_table :item_audits, :audits
  end
end
