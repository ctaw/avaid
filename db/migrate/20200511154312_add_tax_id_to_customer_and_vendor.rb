class AddTaxIdToCustomerAndVendor < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :tax_id, :integer
    add_column :vendors, :tax_id, :integer
  end
end
