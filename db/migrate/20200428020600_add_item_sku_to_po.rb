class AddItemSkuToPo < ActiveRecord::Migration[6.0]
  def change
    drop_table :purchase_orders
    drop_table :purchased_items
    drop_table :suppliers

    create_table :purchase_orders do |t|
      t.integer :vendor_id
      t.integer :receiver_id
      t.string :purchase_order_number
      t.datetime :purchased_date
      t.integer :user_id
      t.decimal :bulk_weight
      t.integer :payment_method_id
      t.timestamps
    end

    create_table :purchase_order_items do |t|
      t.integer :purchase_order_id
      t.string :item_sku
      t.text :description
      t.integer :qty
      t.decimal :rate
      t.decimal :amount
      t.timestamps
    end

    create_table :vendors do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
      t.string :phone_number
      t.string :fax_number
      t.string :contact_name
      t.string :email
      t.timestamps
    end

    create_table :companies do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
      t.string :phone_number
      t.string :fax_number
      t.string :contact_name
      t.string :email
      t.timestamps
    end

  end
end
