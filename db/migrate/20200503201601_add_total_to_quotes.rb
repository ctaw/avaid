class AddTotalToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :total, :decimal
  end
end
