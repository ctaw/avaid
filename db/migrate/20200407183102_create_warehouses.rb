class CreateWarehouses < ActiveRecord::Migration[6.0]
  def change
    create_table :warehouses do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :country_code
      t.string :province_code
      t.string :city_code
      t.timestamps
    end

    rename_column :items, :location, :warehouse_id
  end
end
