class AddItemIdsToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :item_ids, :text, array: true, default: []
    add_column :items, :order_id, :integer
  end
end
