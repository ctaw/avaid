class CreateQuotes < ActiveRecord::Migration[6.0]
  def change
    create_table :quotes do |t|
      t.string :code
      t.integer :customer_id
      t.integer :purchase_order_id
      t.integer :delivery_method_id
      t.integer :delivery_type_id
      t.datetime :date_required
      t.string :eol
      t.integer :user_id
      t.datetime :quote_date
      t.integer :terms_id
      t.integer :department_id
      t.integer :customer_type
      t.boolean :quote_only
      t.boolean :is_rma
      t.boolean :is_coled
      t.boolean :is_shipped
      t.timestamps
    end

    create_table :quote_items do |t|
      t.integer :warranty_id
      t.integer :maker_id
      t.integer :item_id
      t.integer :condition_type_id
      t.text :description
      t.integer :qty
      t.decimal :each_price
      t.timestamps
    end

    create_table :customet_types do |t|
      t.string :name
      t.timestamps
    end

    create_table :delivery_methods do |t|
      t.string :name
      t.timestamps
    end

    create_table :delivery_types do |t|
      t.string :name
      t.timestamps
    end

    create_table :departments do |t|
      t.string :name
      t.timestamps
    end

    create_table :terms do |t|
      t.string :name
      t.timestamps
    end

    create_table :customers do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
      t.string :phone_number
      t.string :fax_number
      t.string :contact_name
      t.string :email
      t.timestamps
    end

    add_column :users, :direct_number, :string

  end
end
