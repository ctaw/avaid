class AddUserIdToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :user_id, :integer
    add_column :orders, :shipped_date, :datetime
    add_column :orders, :status, :string
  end
end
