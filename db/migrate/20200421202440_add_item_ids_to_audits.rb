class AddItemIdsToAudits < ActiveRecord::Migration[6.0]
  def change
    add_column :audits, :item_ids, :text, array: true, default: []
  end
end
