class RemoveQPartNumberToQuotes < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :qpart_number, :string
  end
end
