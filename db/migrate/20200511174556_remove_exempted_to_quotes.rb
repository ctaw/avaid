class RemoveExemptedToQuotes < ActiveRecord::Migration[6.0]
  def change

    remove_column :quotes, :pst_exempted, :boolean
    remove_column :quotes, :gst_exempted, :boolean
    remove_column :quotes, :hst_exempted, :boolean
    add_column :quotes, :tax_id, :integer


  end
end
