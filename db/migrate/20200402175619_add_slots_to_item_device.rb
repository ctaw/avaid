class AddSlotsToItemDevice < ActiveRecord::Migration[6.0]
  def change
    add_column :item_devices, :slots, :string
  end
end
