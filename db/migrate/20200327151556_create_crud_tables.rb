class CreateCrudTables < ActiveRecord::Migration[6.0]
  def change

    create_table :statuses do |t|
      t.string :name
    end

    create_table :condition_types do |t|
      t.string :name
    end

    create_table :categories do |t|
      t.string :name
      t.text :description
    end

    create_table :sub_categories do |t|
      t.integer :category_id
      t.string :name
    end

    create_table :makers do |t|
      t.string :name
      t.text :description
    end

    create_table :maker_models do |t|
      t.integer :maker_id
      t.string :name
    end

    # System, notebook, server, printer, monitor
    create_table :devices do |t|
      t.string :name
    end

    create_table :item_devices do |t|
      t.integer :item_id
      t.integer :device_id
      t.boolean :bootable
      t.string :processor
      t.string :speed
      t.string :hard_disk
      t.string :ram
      t.string :video_card
      t.string :nic
      t.string :cd
      t.string :coa
      t.string :memory_slot
      t.string :memory_type
      t.string :weight
      t.timestamps
    end

    create_table :items do |t|
      t.string :status_id
      t.text :location
      t.integer :category_id
      t.integer :sub_category_id
      t.string :part_number
      t.string :qpart_number
      t.integer :maker_id
      t.integer :maker_model_id
      t.string :condition_type_id
      t.text :description
      t.string :serial
      t.string :hdd_serial
      t.string :grade
      t.string :cost
      t.string :cost_of_addons
      t.string :cost_of_freight
      t.integer :qty
      t.timestamps
    end

    create_table :item_audits do |t|
      t.integer :item_id
      t.text :note
      t.decimal :weight
      t.integer :user_id
      t.datetime :audited_date
      t.string :asset_tag
      t.string :warehouse_tag
      t.string :softchoice_rma_number
      t.timestamps
    end

    create_table :item_ebays do |t|
      t.integer :item_id
      t.decimal :lbs
      t.decimal :lenght
      t.decimal :width
      t.decimal :height
      t.string :country_code
      t.timestamps
    end

  end
end
