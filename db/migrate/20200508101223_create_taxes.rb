class CreateTaxes < ActiveRecord::Migration[6.0]
  def change
    create_table :taxes do |t|
      t.string :code
      t.decimal :percentage
    end

    add_column :purchase_orders, :tax_id, :integer
  end
end
