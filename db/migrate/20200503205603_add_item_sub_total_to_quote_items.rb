class AddItemSubTotalToQuoteItems < ActiveRecord::Migration[6.0]
  def change
    add_column :quote_items, :item_subtotal, :decimal
    remove_column :quotes, :item_subtotal
  end
end
