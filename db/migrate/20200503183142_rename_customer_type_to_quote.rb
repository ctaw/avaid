class RenameCustomerTypeToQuote < ActiveRecord::Migration[6.0]
  def change
    rename_column :quotes, :customer_type, :customer_type_id
  end
end
