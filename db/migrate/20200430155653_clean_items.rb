class CleanItems < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :price, :decimal
    remove_column :items, :old_price, :decimal

    rename_column :items, :supplier_id, :vendor_id
  end
end
