class CreateShippments < ActiveRecord::Migration[6.0]
  def change
    create_table :shipments do |t|
      t.integer :customer_id
      t.integer :quote_id
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
    end

    rename_column :quotes, :purchase_order_id, :quote_purchase_number
    rename_column :quotes, :code, :sku
  end
end
