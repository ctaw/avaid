class TableChangesMay < ActiveRecord::Migration[6.0]
  def change
    remove_column :quote_items, :condition_type_id
    add_column :quote_items, :grade_id, :integer

    remove_column :audits, :asset_tag
    remove_column :audits, :warehouse_tag
    remove_column :audits, :softchoice_rma_number
    remove_column :audits, :weight


    add_column :audits, :customer_id, :integer
    add_column :audits, :audit_date, :datetime
    add_column :audits, :received_date, :datetime
    add_column :audits, :audit_start, :datetime
    add_column :audits, :no_of_items, :integer
    add_column :audits, :received_by, :string
    add_column :audits, :receive_note, :text
  end
end