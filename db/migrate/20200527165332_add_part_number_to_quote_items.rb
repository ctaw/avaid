class AddPartNumberToQuoteItems < ActiveRecord::Migration[6.0]
  def change
    add_column :quote_items, :part_number, :string
  end
end
