class AddNoteToPurchaseOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :purchase_orders, :note, :text
    add_column :purchase_orders, :tax_percentage, :text
    add_column :purchase_orders, :sub_total, :decimal
    add_column :purchase_orders, :total, :decimal
  end
end
