class RenameTermsToQuote < ActiveRecord::Migration[6.0]
  def change
    rename_column :quotes, :terms_id, :term_id
  end
end
