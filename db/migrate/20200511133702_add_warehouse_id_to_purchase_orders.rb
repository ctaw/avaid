class AddWarehouseIdToPurchaseOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :purchase_orders, :warehouse_id, :integer
    drop_table :companies

    add_column :shipments, :warehouse_id, :integer
  end
end
