class RenameVendorIdToPurchaseOrders < ActiveRecord::Migration[6.0]
  def change
    rename_column :purchase_orders, :vendor_id, :supplier_id
  end
end
