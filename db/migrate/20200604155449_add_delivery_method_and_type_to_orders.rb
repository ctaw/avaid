class AddDeliveryMethodAndTypeToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :delivery_method_id, :integer
    add_column :orders, :delivery_type_id, :integer
  end
end
