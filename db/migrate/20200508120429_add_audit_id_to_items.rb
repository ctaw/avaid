class AddAuditIdToItems < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :audit_id, :integer
  end
end
