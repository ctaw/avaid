class ChangeColumnForItemIds < ActiveRecord::Migration[6.0]
  def change
    remove_column :audits, :item_ids
    add_column :audits, :item_ids, :text, array: true, default: []
  end
end