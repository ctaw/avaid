class RenameReceiverIdToCompanyId < ActiveRecord::Migration[6.0]
  def change
    rename_column :purchase_orders, :receiver_id, :company_id
  end
end
