class CreatePaymentMethods < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_methods do |t|
      t.string :name
    end

    rename_column :purchase_orders, :payment_method, :payment_method_id
  end
end
