class CreateWarehouseWorkers < ActiveRecord::Migration[6.0]
  def change
    create_table :warehouse_workers do |t|
      t.integer :warehouse_id
      t.string :name
      t.string :email
    end
  end
end
