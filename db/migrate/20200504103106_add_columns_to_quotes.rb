class AddColumnsToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :gst_amount, :decimal
    add_column :quotes, :pst_amount, :decimal
    add_column :quotes, :hst_amount, :decimal
    add_column :quotes, :subtotal, :decimal
  end
end
