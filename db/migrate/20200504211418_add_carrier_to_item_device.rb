class AddCarrierToItemDevice < ActiveRecord::Migration[6.0]
  def change
    add_column :item_devices, :carrier, :string
  end
end
