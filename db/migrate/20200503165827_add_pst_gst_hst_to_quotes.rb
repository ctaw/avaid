class AddPstGstHstToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :pst_exempted, :boolean
    add_column :quotes, :gst_exempted, :boolean
    add_column :quotes, :hst_exempted, :boolean
    add_column :quotes, :freight, :decimal
  end
end
