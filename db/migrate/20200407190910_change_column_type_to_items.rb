class ChangeColumnTypeToItems < ActiveRecord::Migration[6.0]
  def change
    change_column :items, :warehouse_id, 'integer USING CAST(warehouse_id AS integer)'
  end
end
