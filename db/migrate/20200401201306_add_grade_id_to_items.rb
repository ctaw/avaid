class AddGradeIdToItems < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :grade_id, :integer
  end
end
