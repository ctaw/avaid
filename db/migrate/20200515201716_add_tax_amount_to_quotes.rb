class AddTaxAmountToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :tax_amount, :decimal
    add_column :purchase_orders, :tax_amount, :decimal
  end
end
