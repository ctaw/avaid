class RemoveDevices < ActiveRecord::Migration[6.0]
  def change
    drop_table :devices

    rename_table :item_devices, :item_categories

    rename_column :item_categories, :device_id, :category_id
  end
end
