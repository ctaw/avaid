class AddContactPersonToWarehouses < ActiveRecord::Migration[6.0]
  def change
    add_column :warehouses, :contact_person, :string
  end
end
