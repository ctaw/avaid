class AddPoToItems < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :purchase_order_id, :integer
    add_column :items, :purchase_order_item_sku, :string

    add_column :makers, :sku, :string
    add_column :maker_models, :sku, :string
    add_column :categories, :sku, :string
    add_column :sub_categories, :sku, :string
  end
end
