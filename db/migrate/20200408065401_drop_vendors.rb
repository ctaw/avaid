class DropVendors < ActiveRecord::Migration[6.0]
  def change
    drop_table :vendors

    add_column :suppliers, :address1, :text
    add_column :suppliers, :address2, :text
    add_column :suppliers, :country_code, :string
    add_column :suppliers, :city_code, :string
    add_column :suppliers, :province_code, :string
    add_column :suppliers, :postal_code, :string
    add_column :suppliers, :phone_number, :string
    add_column :suppliers, :fax_number, :string
    add_column :suppliers, :contact_person, :string
    add_column :suppliers, :email, :string
  end
end
