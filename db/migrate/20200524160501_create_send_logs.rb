class CreateSendLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :send_logs do |t|
      t.text :emails
      t.integer :quote_id
      t.timestamps
    end
  end
end
