class AddContactPerson < ActiveRecord::Migration[6.0]
  def change
    add_column :vendors, :contact_person, :string
    add_column :companies, :contact_person, :string
  end
end
