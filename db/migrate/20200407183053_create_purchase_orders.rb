class CreatePurchaseOrders < ActiveRecord::Migration[6.0]
  def change

    add_column :items, :sku, :string
    add_column :items, :price, :decimal
    add_column :items, :old_price, :decimal

    create_table :purchase_orders do |t|
      t.integer :vendor_id
      t.string :reference_number
      t.datetime :purchased_date
      t.integer :created_by
      t.string :payment_method
      t.decimal :bulk_weight
      t.decimal :total_amount

      t.string :name
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
      t.string :phone_number
      t.string :fax_number
      t.string :contact_name
      t.string :email
      t.timestamps
    end

    create_table :purchased_items do |t|
      t.integer :purchase_order_id
      t.string :item_sku
      t.text :description
      t.integer :qty
      t.decimal :item_price
      t.decimal :total_rate
      t.timestamps
    end

  end
end
