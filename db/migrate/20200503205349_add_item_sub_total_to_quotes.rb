class AddItemSubTotalToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :item_subtotal, :decimal
  end
end
