class RemoveItemIdToAudits < ActiveRecord::Migration[6.0]
  def change
    remove_column :audits, :item_id, :integer
  end
end
