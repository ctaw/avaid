class CreateVendors < ActiveRecord::Migration[6.0]
  def change
    create_table :vendors do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :city_code
      t.string :country_code
      t.string :province_code
      t.string :postal_code
      t.string :phone_number
      t.string :fax_number
      t.string :contact_name
      t.string :email
      t.timestamps
    end
  end
end
