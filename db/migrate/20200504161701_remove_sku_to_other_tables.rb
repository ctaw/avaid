class RemoveSkuToOtherTables < ActiveRecord::Migration[6.0]
  def change
    remove_column :makers, :sku
    remove_column :maker_models, :sku
    remove_column :categories, :sku
    remove_column :sub_categories, :sku
  end
end
