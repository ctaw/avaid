class RenameIsColedToQuotes < ActiveRecord::Migration[6.0]
  def change
    rename_column :quotes, :is_coled, :is_closed
  end
end
