class RenameItemSkuToPurchaseOrders < ActiveRecord::Migration[6.0]
  def change
    rename_column :purchased_items, :item_sku, :item_id
    change_column :purchased_items, :item_id, 'integer USING CAST(item_id AS integer)'
  end
end
