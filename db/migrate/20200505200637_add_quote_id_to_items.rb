class AddQuoteIdToItems < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :quote_id, :integer

    add_column :warehouses, :postal_code, :string
    add_column :warehouses, :phone_number, :string
  end
end
