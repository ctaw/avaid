class AddColumnsToItemDevices < ActiveRecord::Migration[6.0]
  def change
    add_column :item_devices, :cd_drive, :string
    add_column :item_devices, :screen_size, :string
    add_column :item_devices, :screen_type, :string
    add_column :item_devices, :modem, :string
    add_column :item_devices, :wireless, :string
    add_column :item_devices, :floppy, :string
    add_column :item_devices, :os, :string
    add_column :item_devices, :no_of_hds, :string
    add_column :item_devices, :hd_size, :string
    add_column :item_devices, :cache, :string
    add_column :item_devices, :type, :string
    add_column :item_devices, :page_count, :integer
    add_column :item_devices, :size, :string
    add_column :item_devices, :manufacture_date, :datetime
    add_column :item_devices, :color, :string
  end
end
