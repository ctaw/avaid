class AddQuoteIdToQuoteItems < ActiveRecord::Migration[6.0]
  def change
    add_column :quote_items, :quote_id, :integer
  end
end
