class AddNoteToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :note, :text
  end
end
