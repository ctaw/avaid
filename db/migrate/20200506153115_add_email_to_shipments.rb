class AddEmailToShipments < ActiveRecord::Migration[6.0]
  def change
    add_column :shipments, :email, :string
    add_column :shipments, :contact_name, :string
  end
end
