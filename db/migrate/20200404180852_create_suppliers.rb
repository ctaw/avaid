class CreateSuppliers < ActiveRecord::Migration[6.0]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :code
      t.text :description
      t.boolean :is_active
      t.timestamps
    end

    add_column :items, :supplier_id, :integer
  end
end
