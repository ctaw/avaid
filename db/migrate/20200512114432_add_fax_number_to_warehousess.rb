class AddFaxNumberToWarehousess < ActiveRecord::Migration[6.0]
  def change
    add_column :warehouses, :fax_number, :string
  end
end
