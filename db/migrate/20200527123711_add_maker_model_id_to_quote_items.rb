class AddMakerModelIdToQuoteItems < ActiveRecord::Migration[6.0]
  def change
    add_column :quote_items, :maker_model_id, :integer
  end
end
