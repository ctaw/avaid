class RenameTypeToItemDevice < ActiveRecord::Migration[6.0]
  def change
    rename_column :item_devices, :type, :d_type
  end
end
