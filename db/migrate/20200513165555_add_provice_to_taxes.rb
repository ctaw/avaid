class AddProviceToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :province_code, :string
  end
end
