class RemoveEolDeliveryMethodDeliveryTypeSkuDeptToQuotes < ActiveRecord::Migration[6.0]
  def change
    remove_column :quotes, :eol
    remove_column :quotes, :delivery_method_id
    remove_column :quotes, :delivery_type_id
    remove_column :quotes, :sku
    remove_column :quotes, :department_id
  end
end
