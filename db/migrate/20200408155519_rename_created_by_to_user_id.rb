class RenameCreatedByToUserId < ActiveRecord::Migration[6.0]
  def change
    rename_column :purchase_orders, :created_by, :user_id
  end
end
