class AddFreightToPurchaseOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :purchase_orders, :freight, :decimal
  end
end
