class AddTaxPercentageToQuotes < ActiveRecord::Migration[6.0]
  def change
    add_column :quotes, :tax_percentage, :decimal
    remove_column :quotes, :gst_amount, :decimal
    remove_column :quotes, :pst_amount, :decimal
    remove_column :quotes, :hst_amount, :decimal
  end
end
