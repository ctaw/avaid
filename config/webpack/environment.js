const { environment } = require('@rails/webpacker')
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const webpack = require('webpack')
environment.plugins.append(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    Popper: ['popper.js', 'default']
  })
)
environment.plugins.prepend("CleanWebpackPlugin", new CleanWebpackPlugin());

module.exports = environment