Rails.application.routes.draw do

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}, :controllers => { sessions: 'sessions'}

  root to: 'home#index'

  namespace :admin do
    resources :dashboard, only: :index

    resources :orders do
      collection do
        get :get_quote
        get :update_maker_model
        get :update_items
        get :get_prev_item
        get :get_next_item
        get :autocomplete
      end
    end

    resources :skus

    resources :audits do
      collection do
        get :lists
        get :get_prev_item
        get :get_next_item
        post :clone
      end
    end

    resources :purchase_orders do
      collection do
        get :get_states
        get :get_cities
        get :get_vendor_details
        get :get_warehouse_details
        post :clone_with_item
        post :clone
        post :add_to_inventory
        get :get_prev_item
        get :get_next_item
        get :get_tax_details
      end
    end

    resources :reports do
      collection do
        get :convert
        get :pdf
      end
    end

    # === Settings ===
    resources :devices
    resources :categories
    resources :makers
    resources :statuses
    resources :order_statuses
    resources :condition_types
    resources :grades
    resources :payment_methods
    resources :delivery_methods
    resources :delivery_types
    resources :customer_types
    resources :terms
    resources :departments
    resources :product_warranties
    resources :product_types
    resources :taxes do
      collection do
        get :get_states
      end
    end
    # === Settings ===

    #  === People ===
    resources :users
    resources :vendors do
      collection do
        get :get_states
        get :get_cities
      end
    end
    # resources :companies do
    #   collection do
    #     get :get_states
    #     get :get_cities
    #   end
    # end
    resources :customers do
      collection do
        get :get_states
        get :get_cities
        get :get_taxes
      end
    end
    #  === People ===

    resources :warehouses do
      collection do
        get :get_states
        get :get_cities
      end
    end

    resources :items do
      collection do
        get :update_maker_model
        get :update_sub_category
        get :set_device
        get :lists
        get :get_prev_item
        get :get_next_item
        post :clone
        get :editable
        get :barcodes
        get :edit_all
        get :get_subcategories
        get :get_makermodels
      end
    end

    resources :quotes do
      collection do
        get :get_customer_details
        get :get_item_details
        get :update_models
        get :update_items
        get :get_states
        get :get_cities
        get :get_states_customer
        get :get_cities_customer
        get :get_tax_details
        get :send_email
        get :approve_quote
        get :get_prev_item
        get :get_next_item
      end
    end

    resources :import_data
    resources :tutorial, only: :index

  end

end
